# %%
import pytest
import numpy as np

# from finesse_ligo.factory import ALIGOFactory, CARMFactory
from finesse_ligo.factory import aligo, carm
from finesse_ligo.suspension import QUADSuspension

# from finesse_ligo.actions import InitialLockLIGO, DARM_RF_to_DC
from finesse.analysis.actions import Series, FrequencyResponse


def test_add_IMC_and_IM1_q():
    factory = aligo.ALIGOFactory("lho_O4.yaml")
    factory.options.INPUT.add_IMC_and_IM1 = True
    lho1 = factory.make()
    factory.options.INPUT.add_IMC_and_IM1 = False
    lho2 = factory.make()

    assert lho1.IM2.p1.i.qx == lho2.IM2.p1.i.qx
    assert lho1.IM2.p1.i.qy == lho2.IM2.p1.i.qy


PARAMETER_FILES = [
    "lho_O4.yaml",
    "llo_O4.yaml",
]

# options changed in factory.options in test_make_and_lock
# loop through keys instead of values so that it's easier to select tests from the cli
TEST_OPTIONS = dict(
    opt0={},
    opt1=[
        ("ASC.add", True),
    ],
    opt2={
        "ASC": {
            "add": True,
            "close_AC_loops": True,
        },
        "LSC": {"close_AC_loops": True},
    },
    opt3={
        "INPUT": {"add_IMC_and_IM1": True},
    },
    opt4={
        "add_118MHz": True,
    },
    opt5={
        "QUAD_suspension_model": QUADSuspension,
    },
    opt6={
        "BS_type": "thin",
    },
    opt7={
        "BS_type": "fake",
    },
    opt8=[
        ("apertures.add", True),
        ("apertures.test_mass", False),  # planewave OK, even_homs fails for True
        ("thermal.add", True),
    ],
    opt9={
        "SQZ": {"add": True},
    },
)

MODE_OPTIONS = {
    "planewave": {"modes": "off"},
    "even_homs": {"modes": "even", "maxtem": 2},
}


@pytest.mark.parametrize("opt", TEST_OPTIONS.keys())
@pytest.mark.parametrize("file", PARAMETER_FILES)
@pytest.mark.parametrize("modes", MODE_OPTIONS.keys())
def test_make_and_lock(file, opt, modes):
    """Quick tests to check that models with different options make, lock, and run a
    simple frequency response."""
    factory = aligo.ALIGOFactory(file)
    factory.update_options(TEST_OPTIONS[opt])

    model = factory.make()
    model.modes(**MODE_OPTIONS[modes])
    model.fsig.f = 1
    model.run(
        Series(
            aligo.InitialLock(),
            aligo.DARM_RF_to_DC(),
            FrequencyResponse(np.geomspace(1, 1e3, 4), ["DARM.AC.i"], ["AS.DC"]),
        )
    )


def test_CARM():
    factory = carm.CARMFactory("lho_O4.yaml")
    model = factory.make()
    model.modes("even", maxtem=2)
    sol = model.run(carm.InitialLock())


def test_input_matrix():
    factory = aligo.ALIGOFactory("lho_O4.yaml")
    factory.options.ASC.add = True
    factory.ASC_input_matrix.DHARD_P["AS_A_WFS45y.Q"] = 1
    factory.ASC_input_matrix.CHARD_P["CHARD_P.AC.i"] = 1
    model1 = factory.make()
    factory.options.ASC.close_AC_loops = True
    model2 = factory.make()


@pytest.fixture
def thin_BS_aligo():
    factory = aligo.ALIGOFactory("lho_O4.yaml")
    factory.options.BS_type = "thin"
    return factory.make()


@pytest.fixture
def fake_BS_aligo():
    factory = aligo.ALIGOFactory("lho_O4.yaml")
    factory.options.BS_type = "fake"
    return factory.make()


@pytest.fixture
def default_aligo():
    factory = aligo.ALIGOFactory("lho_O4.yaml")
    return factory.make()


def test_thin_BS(default_aligo, thin_BS_aligo, fake_BS_aligo):
    assert np.allclose(default_aligo.l_schnupp.eval(), thin_BS_aligo.l_schnupp.eval())
    assert np.allclose(default_aligo.l_schnupp.eval(), fake_BS_aligo.l_schnupp.eval())
    with pytest.raises(AttributeError):
        print(fake_BS_aligo.BSARX)
    with pytest.raises(AttributeError):
        print(fake_BS_aligo.BSARSR)
    with pytest.raises(AttributeError):
        print(fake_BS_aligo.subBS_X)
    with pytest.raises(AttributeError):
        print(fake_BS_aligo.subBS_SR)


# %%
