# %%
import numpy as np
import finesse
import finesse_ligo
from finesse_ligo.factory import ALIGOFactory
from finesse_ligo.actions import InitialLockLIGO, DARM_RF_to_DC
from finesse.plotting import bode
import finesse.analysis.actions as fac

finesse.init_plotting()

# We first make a factory object that can generate an ALIGO model
# here we do so using the LHO O4 parameter file
factory = ALIGOFactory("lho_O4.yaml")

# %%
# The factory now produces a model that can be used
factory.reset()
lho = factory.make()
lho.modes("off")  # planewave
lho.run(fac.Series(InitialLockLIGO(), DARM_RF_to_DC()))

# %% Now make a new LIGO model with suspensions
factory.reset()
factory.options.QUAD_suspension_model = finesse_ligo.suspension.QUADSuspension

lho2 = factory.make()
lho2.modes("off")
lho2.run(fac.Series(InitialLockLIGO(), DARM_RF_to_DC()))

# %% Make a HOM model
factory.reset()
factory.options.QUAD_suspension_model = finesse_ligo.suspension.QUADSuspension
lho3 = factory.make()
lho3.modes("even", maxtem=2)
lho3.run(fac.Series(InitialLockLIGO(), DARM_RF_to_DC()))

# %%
analysis = fac.FrequencyResponse(
    np.geomspace(0.1, 10e3, 300), "DARM.AC.i", "AS.DC", open_loop=True
)

sol1 = lho.run(analysis)
sol2 = lho2.run(analysis)
sol3 = lho3.run(analysis)

# %%
axs = bode(sol1.f, sol1.out[:, 0, 0], db=False, label="No QUADs+planewave")
bode(sol2.f, sol2.out[:, 0, 0], db=False, axs=axs, ls="--", label="QUADs+planewave")
bode(sol3.f, sol3.out[:, 0, 0], db=False, axs=axs, ls="-.", label="QUADs+HOMs")
axs[0].set_title("DARM sensing function")
axs[0].set_ylabel("Magnitude [W/m]")


# factory.local_drives.L.ETMX.clear() # get rid of what's there
# # Now we add in that we want to drive M0 and L2
# # Note that we use a 4-tuple (4-element tuple): (name, z, p ,k)
# # name is what the ZPKFilter element will be called in the model that is made
# factory.local_drives.L.ETMX['ETMX_sus.M0.F_z'] = ("zpk_EX_M0", [], [], 1)
# factory.local_drives.L.ETMX['ETMX_sus.L2.F_z'] = ("zpk_EX_L2", [], [], 1)
# # Do the same for ETMY
# factory.local_drives.L.ETMY.clear()
# factory.local_drives.L.ETMY['ETMY_sus.M0.F_z'] = ("zpk_EY_M0", [], [], 1)
# factory.local_drives.L.ETMY['ETMY_sus.L2.F_z'] = ("zpk_EY_L2", [], [], 1)

# # Let's say we want to mix two inputs to make the appropriate error signal
# # these can be a single value or a 4-tuple, here it's just a simple sum
# factory.LSC_input_matrix.PRCL.clear()
# factory.LSC_input_matrix.PRCL.REFL9_I = 1
# factory.LSC_input_matrix.PRCL.REFL45_I = 1

# # We can also add a ZPK controller, this must be a 4-tuple
# # If we don't specify one here then there is no controller
# factory.LSC_controller.PRCL = ('ctrl_PRCL', [], [], 1)

# factory.ASC_input_matrix.DHARD_P.AS_A_WFS45y_Q = 1
# factory.ASC_input_matrix.CHARD_P.REFL_B_WFS9y_I = 1
# factory.ASC_input_matrix.CHARD_P.REFL_B_WFS45y_I = 1

# factory.ASC_input_matrix.DHARD_Y.AS_A_WFS45x_Q = 1
# factory.ASC_input_matrix.CHARD_Y.REFL_B_WFS9x_I = 1
# factory.ASC_input_matrix.CHARD_Y.REFL_B_WFS45x_I = 1

# factory.ASC_controller.CHARD_P = ('ctrl_CHARD_P', [], [], 1)
# factory.ASC_controller.DHARD_P = ('ctrl_DHARD_P', [], [], 1)
# factory.ASC_controller.CHARD_Y = ('ctrl_CHARD_Y', [], [], 1)
# factory.ASC_controller.DHARD_Y = ('ctrl_DHARD_Y', [], [], 1)
