# %%
import numpy as np
import finesse
import finesse_ligo
from finesse_ligo.factory import ALIGOFactory
from finesse_ligo.actions import InitialLockLIGO, DARM_RF_to_DC
from finesse.plotting import bode
import finesse.analysis.actions as fac

finesse.init_plotting()


def setup_blend(factory, TM, f_crossover):
    # 1Hz cross over
    M0, L2 = cross_over_tfs(f_crossover)
    factory.local_drives.L[TM].clear()
    # Use a 4-tuple to add a new zpk filter
    factory.local_drives.L[TM][f"{TM}_sus.M0.F_z"] = (f"blend_{TM}_M0", *M0)
    factory.local_drives.L[TM][f"{TM}_sus.L2.F_z"] = (f"blend_{TM}_L2", *L2)


def cross_over_tfs(f_cross):
    "Make two crossover filters"
    w_cross_rad = 2 * np.pi * f_cross
    lower = ([], [-w_cross_rad], w_cross_rad)
    upper = ([0], [-w_cross_rad], 1)
    return lower, upper


# %% We first make a factory object that can generate an ALIGO model
# here we do so using the LHO O4 parameter file
factory = ALIGOFactory("lho_O4.yaml")

# %% And make a standard model
factory.reset()
factory.options.QUAD_suspension_model = finesse_ligo.suspension.QUADSuspension
lho0 = factory.make()
lho0.modes("off")  # planewave
lho0.run(fac.Series(InitialLockLIGO(), DARM_RF_to_DC()))

# %% We first make a factory object that can generate an ALIGO model
# here we do so using the LHO O4 parameter file
factory = ALIGOFactory("lho_O4.yaml")
# %% The factory is now set up to make a model that drives different
# suspension points
factory.reset()
# Make sure we have the quads in the model
factory.options.QUAD_suspension_model = finesse_ligo.suspension.QUADSuspension
# Now go and setup the blend filters
setup_blend(factory, "ITMX", 1)
setup_blend(factory, "ETMX", 1)
setup_blend(factory, "ITMY", 1)
setup_blend(factory, "ETMY", 1)
lho = factory.make()
# Run initial locking routine and move to DC readout
lho.modes("off")  # planewave
lho.run(fac.Series(InitialLockLIGO(), DARM_RF_to_DC()))

# %% Plot the signal flow to see that things are connected up properly
lho.display_signal_blockdiagram("DARM.AC.i", "DARM.AC.o")

# %% Plot the cross over actuation
sol = lho.run(
    fac.FrequencyResponse(
        np.geomspace(0.1, 10, 200),
        ["blend_ITMX_M0.p1", "blend_ITMX_L2.p1"],
        ["ITMX_sus.L3.z"],
        open_loop=True,
    )
)
axs = bode(sol.f, sol.out[:, 0], label="L3/M0", db=False)
axs = bode(sol.f, sol.out[:, 1], axs=axs, label="L3/L2", db=False)
axs[0].set_title("Crossover")
axs[0].set_ylabel("Magnitude [m/N]")

# %%
