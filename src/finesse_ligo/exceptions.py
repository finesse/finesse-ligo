from finesse.exceptions import FinesseException


class InitialLockCheckException(FinesseException):
    """Exception raised when the initial lock check fails."""

    pass
