from .tools import download
from . import thermal
from . import suspension
from . import actions
from . import maps
from . import asc
from . import factory
from . import exceptions

import importlib.resources
import pathlib


def git_path():
    """Returns a Path object for the root of the current Git repository the code is
    being run in.

    Examples
    --------
    Say we are in `/Users/user/git/finesse-ligo/tests/asc` and we call
    >>> finesse_ligo.git_path()
    PosixPath('/Users/user/git/finesse-ligo')

    If we want to select a known directory within our git repository, say to
    store some Matplotlib figure results in we can do:
    >>> finesse_ligo.git_path() / 'document' / 'figures'
    PosixPath('/Users/user/git/finesse-ligo/document/figure')

    This ensures that anyone else who runs this script will always save to
    the correcy place, regardless of where they have cloned the repository.

    Raises
    ------
    Raises a InvalidGitRepositoryError if it is called in a folder that
    is not a git repository, at is is looking for the top-level `.git`.
    """
    import git

    git_repo = git.Repo(".", search_parent_directories=True)
    git_root = pathlib.Path(git_repo.git.rev_parse("--show-toplevel"))
    return git_root


# Set the finesse_ligo version.
try:
    from ._version import version as __version__
except ImportError:
    raise Exception("Could not find version.py. Ensure you have run setup.")

aligo_katscript = (
    importlib.resources.files("finesse_ligo.katscript")
    .joinpath("aligo.kat")
    .read_text()
)

try:
    # KATSPEC has been made a singleton in later versions
    from finesse.script.spec import KATSPEC as spec

    from packaging import version
    from finesse import __version__ as finesse_version

    if "+" in finesse_version:
        finesse_version = finesse_version.split("+")[0]

    require_finesse = "3.0a23.dev17"
    if version.parse(finesse_version) < version.parse(require_finesse):
        raise Exception(
            f"You need at least Finesse {require_finesse} or higher to run this finesse-ligo, you have {finesse_version}"
        )

except ImportError:
    from finesse.script.spec import KatSpec

    spec = KatSpec()  # grabs existing instance

from finesse.script.spec import make_element, make_analysis

spec.register_element(
    make_element(suspension.LIGOTripleSuspension, "ligo_triple"), overwrite=True
)
spec.register_element(
    make_element(suspension.LIGOQuadSuspension, "ligo_quad"), overwrite=True
)
spec.register_analysis(
    make_analysis(actions.DARM_RF_to_DC, "darm_rf_to_dc"), overwrite=True
)
spec.register_analysis(
    make_analysis(actions.DRFPMI_state, "drfpmi_state"), overwrite=True
)


__all__ = (
    "download",
    "thermal",
    "suspension",
    "actions",
    "maps",
    "asc",
    "factory",
    "__version__",
    "exceptions",
)
