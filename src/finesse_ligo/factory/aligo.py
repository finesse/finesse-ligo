import numpy as np
from munch import Munch
import finesse
import finesse.components as fc
from finesse.detectors import MathDetector, PowerDetector, AmplitudeDetector
from finesse.symbols import Constant
import finesse.analysis.actions as fa

from .base import DRFPMIFactory
from .. import maps


class ALIGOFactory(DRFPMIFactory):
    def reset(self):
        """Resets factory to all default options, drives, and LSC/ASC settings."""
        self.set_default_options()
        self.set_default_drives()
        self.set_default_LSC()
        self.set_default_ASC()

    def set_default_options(self):
        """Resets factory to all default options."""

        super().set_default_options()

        # self.options defined in DRFPMIFactory

        # self.options.INPUT defined in DRFPMIFactory

        # self.options.LSC defined in DRFPMIFactory
        self.options.LSC.add_DOFs = True
        self.options.LSC.add_readouts = True
        self.options.LSC.add_output_detectors = True
        self.options.LSC.add_locks = True
        self.options.LSC.add_AC_loops = True
        self.options.LSC.close_AC_loops = False

        self.options.ASC = Munch()
        self.options.ASC.add = False
        self.options.ASC.add_DOFs = True
        self.options.ASC.add_readouts = True
        self.options.ASC.add_output_detectors = True
        self.options.ASC.add_locks = True
        self.options.ASC.add_AC_loops = True
        self.options.ASC.close_AC_loops = False

        self.options.SQZ = Munch()
        self.options.SQZ.add = False
        self.options.SQZ.add_detectors = False
        self.options.SQZ.add_VIP = False
        self.options.SQZ.add_seed_laser = False

        self.options.apertures = Munch()
        self.options.apertures.add = False
        self.options.apertures.test_mass = True
        self.options.apertures.BS_ITM = False
        self.options.apertures.BS_HR = False
        self.options.apertures.PR3_SR3 = True
        self.options.apertures.make_axisymmetric = False
        self.options.apertures.use_surface_profile = False
        self.options.thermal = Munch(add=False)

    def set_default_drives(self, L_force=False, P_torque=True, Y_torque=True):
        """Resets factory to all default drives.

        Parameters
        ----------
        L_force : bool
            Whether the length drive should be a force (True) or a
            displacement (False). Default: False.
        P_torque : bool
            Whether the pitch drive should be a torque (True) or an
            angle (False). Default: True.
        Y_torque : bool
            Whether the yaw drive should be a torque (True) or an
            angle (False). Default: True.
        """
        Ltype = "F_" if L_force else ""
        Ptype = "F_" if P_torque else ""
        Ytype = "F_" if Y_torque else ""
        self.local_drives = Munch(
            {
                "L": Munch(
                    {
                        "ITMX": {f"ITMX.mech.{Ltype}z": 1},
                        "ETMX": {f"ETMX.mech.{Ltype}z": 1},
                        "ITMY": {f"ITMY.mech.{Ltype}z": 1},
                        "ETMY": {f"ETMY.mech.{Ltype}z": 1},
                        "PRM": {f"PRM.mech.{Ltype}z": 1},
                        "SRM": {f"SRM.mech.{Ltype}z": 1},
                        "BS": {f"BS.mech.{Ltype}z": 1},
                    }
                ),
                "P": Munch(
                    {
                        "ITMX": {f"ITMX.mech.{Ptype}pitch": 1},
                        "ETMX": {f"ETMX.mech.{Ptype}pitch": 1},
                        "ITMY": {f"ITMY.mech.{Ptype}pitch": 1},
                        "ETMY": {f"ETMY.mech.{Ptype}pitch": 1},
                        "PRM": {f"PRM.mech.{Ptype}pitch": 1},
                        "PR2": {f"PR2.mech.{Ptype}pitch": 1},
                        "SRM": {f"SRM.mech.{Ptype}pitch": 1},
                        "SR2": {f"SR2.mech.{Ptype}pitch": 1},
                        "BS": {f"BS.mech.{Ptype}pitch": 1},
                        "IM4": {f"IM4.mech.{Ptype}pitch": 1},
                    }
                ),
                "Y": Munch(
                    {
                        "ITMX": {f"ITMX.mech.{Ytype}yaw": 1},
                        "ETMX": {f"ETMX.mech.{Ytype}yaw": 1},
                        "ITMY": {f"ITMY.mech.{Ytype}yaw": 1},
                        "ETMY": {f"ETMY.mech.{Ytype}yaw": 1},
                        "PRM": {f"PRM.mech.{Ytype}yaw": 1},
                        "PR2": {f"PR2.mech.{Ytype}yaw": 1},
                        "SRM": {f"SRM.mech.{Ytype}yaw": 1},
                        "SR2": {f"SR2.mech.{Ytype}yaw": 1},
                        "BS": {f"BS.mech.{Ytype}yaw": 1},
                        "IM4": {f"IM4.mech.{Ytype}yaw": 1},
                    }
                ),
            }
        )

    def set_default_LSC(self):
        """Resets factory to all default LSC settings."""
        self.LSC_output_matrix = Munch(
            {
                "XARM": Munch(),
                "YARM": Munch(),
                "CARM": Munch(),
                "DARM": Munch(),
                "PRCL": Munch(),
                "SRCL": Munch(),
                "MICH": Munch(),
                "MICH2": Munch(),
            }
        )

        # Note that if lx, ly, Lp, and Ls are the distances between the BS and ITMX, the
        # BS and ITMY, the PRC, and the SRC, respectively, then the MICH defined in this
        # output matrix is really driving lm = lx - ly = MICH as well as
        # Lp = -MICH and Ls = +MICH and that the MICH2 defined in this output matrix is
        # driving lm = MICH2 and no other degrees of freedom
        self.LSC_output_matrix.XARM.ETMX = +1
        self.LSC_output_matrix.YARM.ETMY = +1
        self.LSC_output_matrix.CARM.ETMX = +0.5
        self.LSC_output_matrix.CARM.ETMY = +0.5
        self.LSC_output_matrix.DARM.ETMX = +1
        self.LSC_output_matrix.DARM.ETMY = -1
        self.LSC_output_matrix.PRCL.PRM = +1
        self.LSC_output_matrix.SRCL.SRM = +1
        self.LSC_output_matrix.MICH.BS = +np.sqrt(2)
        self.LSC_output_matrix.MICH2.BS = +np.sqrt(2)
        self.LSC_output_matrix.MICH2.PRM = -1
        self.LSC_output_matrix.MICH2.SRM = +1

        self.LSC_input_matrix = Munch(
            {
                "XARM": Munch(),
                "YARM": Munch(),
                "CARM": Munch(),
                "DARM": Munch(),
                "PRCL": Munch(),
                "SRCL": Munch(),
                "MICH": Munch(),
                "MICH2": Munch(),
            }
        )

        self.LSC_input_matrix.DARM["AS.DC"] = 1
        self.LSC_input_matrix.CARM["REFL9.I"] = 1
        self.LSC_input_matrix.MICH2["POP45.Q"] = 1
        self.LSC_input_matrix.SRCL["POP45.I"] = 1
        self.LSC_input_matrix.PRCL["POP9.I"] = 1

        self.LSC_controller = Munch()

    def set_default_ASC(self):
        """Resets factory to all default ASC settings."""
        # See https://dcc.ligo.org/LIGO-D2200425/public for a diagram of arm modes.
        self.ASC_output_matrix = Munch()
        self.ASC_output_matrix.CHARD_P = Munch(
            {"ITMX": -1, "ETMX": "+rx_P", "ITMY": -1, "ETMY": "+ry_P"}
        )
        self.ASC_output_matrix.DHARD_P = Munch(
            {"ITMX": -1, "ETMX": "+rx_P", "ITMY": +1, "ETMY": "-ry_P"}
        )
        self.ASC_output_matrix.CSOFT_P = Munch(
            {"ITMX": "+rx_P", "ETMX": +1, "ITMY": "+ry_P", "ETMY": +1}
        )
        self.ASC_output_matrix.DSOFT_P = Munch(
            {"ITMX": "+rx_P", "ETMX": +1, "ITMY": "-ry_P", "ETMY": -1}
        )
        self.ASC_output_matrix.CHARD_Y = Munch(
            {"ITMX": +1, "ETMX": "+rx_Y", "ITMY": -1, "ETMY": "-ry_Y"}
        )
        self.ASC_output_matrix.DHARD_Y = Munch(
            {"ITMX": +1, "ETMX": "+rx_Y", "ITMY": +1, "ETMY": "+ry_Y"}
        )
        self.ASC_output_matrix.CSOFT_Y = Munch(
            {"ITMX": "+rx_Y", "ETMX": -1, "ITMY": "-ry_Y", "ETMY": +1}
        )
        self.ASC_output_matrix.DSOFT_Y = Munch(
            {"ITMX": "+rx_Y", "ETMX": -1, "ITMY": "+ry_Y", "ETMY": -1}
        )

        self.ASC_output_matrix.SRC1_P = Munch({"SRM": +1})
        self.ASC_output_matrix.SRC1_Y = Munch({"SRM": +1})
        self.ASC_output_matrix.SRC2_P = Munch({"SRM": +1, "SR2": -7.6})
        self.ASC_output_matrix.SRC2_Y = Munch({"SRM": +1, "SR2": +7.1})
        self.ASC_output_matrix.MICH_P = Munch({"BS": +1})
        self.ASC_output_matrix.MICH_Y = Munch({"BS": +1})

        self.ASC_output_matrix.INP1_P = Munch({"IM4": +1})
        self.ASC_output_matrix.INP1_Y = Munch({"IM4": +1})
        self.ASC_output_matrix.PRC2_P = Munch({"PR2": +1})
        self.ASC_output_matrix.PRC2_Y = Munch({"PR2": +1})

        self.ASC_input_matrix = Munch(
            {
                "CHARD_P": Munch(),
                "DHARD_P": Munch(),
                "CSOFT_P": Munch(),
                "DSOFT_P": Munch(),
                "CHARD_Y": Munch(),
                "DHARD_Y": Munch(),
                "CSOFT_Y": Munch(),
                "DSOFT_Y": Munch(),
                "SRC1_P": Munch(),
                "SRC1_Y": Munch(),
                "SRC2_P": Munch(),
                "SRC2_Y": Munch(),
                "MICH_P": Munch(),
                "MICH_Y": Munch(),
                "INP1_P": Munch(),
                "INP1_Y": Munch(),
                "PRC2_P": Munch(),
                "PRC2_Y": Munch(),
            }
        )

        self.ASC_controller = Munch()

    def make(self):
        base = finesse.Model()
        self.pre_make(base, self.params)

        self.add_arm_cavity(base, "X", self.params.X, self.options)
        self.add_arm_cavity(base, "Y", self.params.Y, self.options)
        self.add_BS(base, self.params.BS)
        self.add_MICH(base, "X", self.params.X, self.BS_X, self.ITMX_AR_fr)
        self.add_MICH(base, "Y", self.params.Y, self.BS_Y, self.ITMY_AR_fr)
        self.add_PRC(base, self.params.PRC, self.BS_PR, self.options)
        self.add_SRC(base, self.params.SRC, self.BS_SR, self.options)

        self.add_OMC(base, self.params.OMC)

        self.add_input_path(base, self.params.INPUT, self.PRM_AR_fr)
        self.add_output_path(
            base, self.params.OUTPUT, self.SRM_AR_fr, self.OMC_input_port
        )

        self.add_cavities(base)

        self.add_suspensions(base)
        self.add_LSC(base)
        if self.options.ASC.add:
            self.add_ASC(base)
        if self.options.SQZ.add:
            self.add_squeeze_path(base, self.params.SQZ, self.OFI_sqz_port)
        if self.options.add_detectors:
            self.add_detectors(base)
        if self.options.apertures.add:
            self.add_apertures(base)
        if self.options.thermal.add:
            self.add_test_mass_thermal(base)
        if self.options.add_transmon:
            self.add_transmon(base)

        self.post_make(base, self.params)
        base.beam_trace()
        return base

    def pre_make(self, model, params):
        pass

    def add_cavities(self, model):
        model.add(fc.Cavity("cavXARM", model.ETMX.p1.o, priority=100))
        model.add(fc.Cavity("cavYARM", model.ETMY.p1.o, priority=100))
        model.add(fc.Cavity("cavPRX", self.PRM_HR_fr.o, via=self.ITMX_HR_bk.i))
        model.add(fc.Cavity("cavPRY", self.PRM_HR_fr.o, via=self.ITMY_HR_bk.i))
        model.add(
            fc.Cavity(
                "cavSRX",
                self.SRM_HR_fr.o,
                via=self.ITMX_HR_bk.i,
                priority=-50,
            )
        )
        model.add(
            fc.Cavity(
                "cavSRY",
                self.SRM_HR_fr.o,
                via=self.ITMY_HR_bk.i,
                priority=-50,
            )
        )

    def add_LSC(self, model):
        super().add_LSC(model)
        if self.options.LSC.add_DOFs:
            model.add(
                fc.DegreeOfFreedom("STRAIN", model.LX.dofs.h, +1, model.LY.dofs.h, -1)
            )

    def add_suspensions(self, model):
        if self.options.QUAD_suspension_model is not None:
            kwargs = self.options.QUAD_suspension_kwargs
            element = self.options.QUAD_suspension_model

            model.add(element("ITMX_sus", model.get("ITMX.mech"), **kwargs))
            model.add(element("ITMY_sus", model.get("ITMY.mech"), **kwargs))
            model.add(element("ETMX_sus", model.get("ETMX.mech"), **kwargs))
            model.add(element("ETMY_sus", model.get("ETMY.mech"), **kwargs))

    def add_LSC_readouts(self, model):
        f1 = model.f1.ref
        f2 = model.f2.ref
        output_detectors = self.options.LSC.add_output_detectors
        model.add(
            fc.ReadoutRF(
                "REFL9", self.REFL_port.o, f=f1, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "REFL45", self.REFL_port.o, f=f2, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "POP9", self.POP_port.o, f=f1, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "POP18", self.POP_port.o, f=2 * f1, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "POP36", self.POP_port.o, f=f2 - f1, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "POP45", self.POP_port.o, f=f2, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "POP90", self.POP_port.o, f=2 * f2, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "AS45", self.AS_port.o, f=f2, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "AS36", self.AS_port.o, f=f2 - f1, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutDC(
                "AS", self.OMC_output_port.o, output_detectors=output_detectors
            )
        )

    def add_LSC_DC_locks(self, model):
        model.add(
            finesse.locks.Lock(
                "CARM_lock", model.REFL9.outputs.I, model.CARM.DC, -0.1, 1e-6
            )
        )
        model.add(
            finesse.locks.Lock(
                "MICH_lock", model.POP45.outputs.Q, model.MICH.DC, -15, 1e-6
            )
        )
        model.add(
            finesse.locks.Lock(
                "PRCL_lock", model.POP9.outputs.I, model.PRCL.DC, 2.8, 1e-6
            )
        )
        model.add(
            finesse.locks.Lock(
                "SRCL_lock", model.POP45.outputs.I, model.SRCL.DC, 42, 1e-6
            )
        )
        model.add(
            finesse.locks.Lock(
                "DARM_rf_lock", model.AS45.outputs.Q, model.DARM.DC, -0.003, 1e-6
            )
        )
        model.add(
            finesse.locks.Lock(
                "DARM_dc_lock",
                model.AS.outputs.DC,
                model.DARM.DC,
                -0.003,
                1e-6,
                offset=20e-3,
                enabled=False,
            )
        )

    def add_LSC_AC_loops(self, model):
        self._add_AC_loops(
            model,
            self.local_drives["L"],
            self.LSC_input_matrix,
            self.LSC_output_matrix,
            self.LSC_controller,
            self.options.LSC,
        )

    def add_ASC_AC_loops(self, model):
        for Y_P in ["Y", "P"]:
            self._add_AC_loops(
                model,
                self.local_drives[Y_P],
                dict(
                    filter(
                        lambda x: x[0].endswith("_" + Y_P),
                        self.ASC_input_matrix.items(),
                    )
                ),
                dict(
                    filter(
                        lambda x: x[0].endswith("_" + Y_P),
                        self.ASC_output_matrix.items(),
                    )
                ),
                dict(
                    filter(
                        lambda x: x[0].endswith("_" + Y_P),
                        self.ASC_controller.items(),
                    )
                ),
                self.options.ASC,
            )

    def add_ASC_readouts(self, model):
        f1 = model.f1.ref
        f2 = model.f2.ref
        f3 = model.f3.ref
        output_detectors = self.options.ASC.add_output_detectors

        def add_WFS(f, port, fkey, port_prefix):
            model.add(
                fc.ReadoutRF(
                    f"{port}_A_WFS{fkey}x",
                    optical_node=model.get(f"{port_prefix}{port}_A").p1.i,
                    f=f,
                    pdtype="xsplit",
                    output_detectors=output_detectors,
                )
            )
            model.add(
                fc.ReadoutRF(
                    f"{port}_A_WFS{fkey}y",
                    optical_node=model.get(f"{port_prefix}{port}_A").p1.i,
                    f=f,
                    pdtype="ysplit",
                    output_detectors=output_detectors,
                )
            )
            model.add(
                fc.ReadoutRF(
                    f"{port}_B_WFS{fkey}x",
                    optical_node=model.get(f"{port_prefix}{port}_B").p1.i,
                    f=f,
                    pdtype="xsplit",
                    output_detectors=output_detectors,
                )
            )
            model.add(
                fc.ReadoutRF(
                    f"{port}_B_WFS{fkey}y",
                    optical_node=model.get(f"{port_prefix}{port}_B").p1.i,
                    f=f,
                    pdtype="ysplit",
                    output_detectors=output_detectors,
                )
            )

        # create WFS A and B at placeholder location
        # only includes 45 and 36  MHz WFS (used for DHARD and MICH ASC)
        add_WFS(f2, "AS", "45", "")
        add_WFS(f2 - f1, "AS", "36", "")
        add_WFS(f3 - f2, "AS", "72", "")

        # AS_C QPD created at placeholder
        model.add(
            fc.ReadoutDC(
                "AS_Cx",
                optical_node=model.AS_C.p1.i,
                pdtype="xsplit",
                output_detectors=output_detectors,
            )
        )
        model.add(
            fc.ReadoutDC(
                "AS_Cy",
                optical_node=model.AS_C.p1.i,
                pdtype="ysplit",
                output_detectors=output_detectors,
            )
        )

        # place ASC REFL WFS, 9 and 45 MHz
        add_WFS(f1, "REFL", "9", "ASC_")
        add_WFS(f2, "REFL", "45", "ASC_")

    def add_transmon(self, model):
        # Following T1000247-v3; T0900385-v6
        params = self.params.TRANSMON
        for arm in ["X", "Y"]:
            # Steering the beam (keeping only one TS{arm}_M1 at a distance of 2m instead
            # of SM1 and SM2)
            TS_M1 = model.add(
                fc.Beamsplitter(
                    f"TS{arm}_M1",
                    R=1,
                    T=0,
                    alpha=params.M1.AOI,
                    Rc=params.M1.Rc,
                )
            )
            TS_M2 = model.add(
                fc.Beamsplitter(
                    f"TS{arm}_M2",
                    R=1,
                    T=0,
                    alpha=TS_M1.alpha.ref,
                    Rc=params.M2.Rc,
                )
            )
            IQPD_L1 = model.add(fc.Lens(f"IQPD{arm}_L1", f=params.L1.f))
            IQPD_L2 = model.add(fc.Lens(f"IQPD{arm}_L2", f=params.L2.f))
            model.connect(
                model.get(f"ETM{arm}").p2,
                TS_M1.p1,
                L=params.length_ETM_M1,
                name=f"s_TMON{arm}_1",
            )
            model.connect(
                TS_M1.p2,
                TS_M2.p1,
                L=params.length_M1_M2,
                name=f"s_TMON_{arm}_2",
            )
            model.connect(
                TS_M2.p2,
                IQPD_L1.p1,
                L=params.length_M2_L1,
                name=f"s_TMON{arm}_3",
            )
            model.connect(
                IQPD_L1.p2,
                IQPD_L2.p1,
                L=params.length_L1_L2,
                name=f"s_TMON{arm}_4",
            )

            # splitting the beam for two QPDs with a mirror
            TS_M3 = model.add(fc.Beamsplitter(f"TS{arm}_M3", R=0.5, T=0.5))
            model.connect(
                IQPD_L2.p2,
                TS_M3.p1,
                L=params.length_L2_M3,
                name=f"s_TMON{arm}_5",
            )

            # QPDs
            QPD1 = model.add(fc.Nothing(f"IQPD{arm}_QPD1"))
            QPD2 = model.add(fc.Nothing(f"IQPD{arm}_QPD2"))
            model.connect(
                TS_M3.p2,
                QPD1.p1,
                L=params.length_M3_QPD1,
                name=f"s_TMON{arm}_6",
            )
            model.connect(
                TS_M3.p3,
                QPD2.p1,
                L=params.length_M3_QPD2,
                name=f"s_TMON{arm}_7",
            )
            model.add(
                fc.ReadoutDC(
                    f"QPD{arm}_1x",
                    optical_node=QPD1.p1.i,
                    pdtype="xsplit",
                    output_detectors=True,
                )
            )
            model.add(
                fc.ReadoutDC(
                    f"QPD{arm}_1y",
                    optical_node=QPD1.p1.i,
                    pdtype="ysplit",
                    output_detectors=True,
                )
            )
            model.add(
                fc.ReadoutDC(
                    f"QPD{arm}_2x",
                    optical_node=QPD2.p1.i,
                    pdtype="xsplit",
                    output_detectors=True,
                )
            )
            model.add(
                fc.ReadoutDC(
                    f"QPD{arm}_2y",
                    optical_node=QPD2.p1.i,
                    pdtype="ysplit",
                    output_detectors=True,
                )
            )

    def add_detectors(self, model):
        f1 = model.f1.ref
        f2 = model.f2.ref

        def add_amplitude_detectors(port, key):
            freqs = []
            if self.options.add_9MHz:
                freqs.append((f1, "9"))
            if self.options.add_45MHz:
                freqs.append((f2, "45"))
            self._add_all_amplitude_detectors(model, port, key, *freqs)

        model.add(PowerDetector("Pin", self.PRM_AR_fr.i))
        Px = Constant(model.Px)
        Py = Constant(model.Py)

        model.add(PowerDetector("PreflPRM", self.PRM_AR_fr.o))
        model.add(PowerDetector("Prefl", self.REFL_port.o))
        model.add(PowerDetector("Ppop", self.POP_port.o))
        model.add(PowerDetector("Pas_c", model.OM1.p1.i))
        model.add(PowerDetector("Pas", self.OMC_output_port.o))
        Pinx = Constant(model.Pinx)
        Piny = Constant(model.Piny)

        add_amplitude_detectors(self.POP_port.o, "pop")
        add_amplitude_detectors(self.OMC_output_port.o, "dcpd")
        add_amplitude_detectors(self.AS_port.o, "as")
        add_amplitude_detectors(model.PRM.p2.i, "in")
        add_amplitude_detectors(self.REFL_port.o, "refl")
        add_amplitude_detectors(model.PRM.p1.o, "prc")
        freqs = [
            (f1, "9"),
            (f2, "45"),
            (f2, "45", 0, 0),
        ]
        self._add_all_amplitude_detectors(model, model.SRM.p1.i, "src", *freqs)

        model.add(AmplitudeDetector("a_carrier_refl", self.REFL_port.o, f=0))
        a_carrier_pinx = Constant(model.a_carrier_pinx)
        a_carrier_piny = Constant(model.a_carrier_piny)
        a_carrier_x = Constant(model.a_carrier_x)
        a_carrier_y = Constant(model.a_carrier_y)

        # FIXME: there should be some logic below for if 9 and 45 MHz have been added
        Pprc_carrier = abs(Constant(model.a_c0_prc)) ** 2
        Pin_carrier = abs(Constant(model.a_c0_in)) ** 2
        Pprc_9 = abs(Constant(model.a_u9_prc)) ** 2 + abs(Constant(model.a_l9_prc)) ** 2
        Pprc_45 = (
            abs(Constant(model.a_u45_prc)) ** 2 + abs(Constant(model.a_l45_prc)) ** 2
        )
        Pin_9 = abs(Constant(model.a_u9_in)) ** 2 + abs(Constant(model.a_l9_in)) ** 2
        Pin_45 = abs(Constant(model.a_u45_in)) ** 2 + abs(Constant(model.a_l45_in)) ** 2
        Pinx_carrier = abs(a_carrier_pinx) ** 2
        Piny_carrier = abs(a_carrier_piny) ** 2

        model.add(MathDetector("Pprc_carrier", Pprc_carrier))
        model.add(MathDetector("Pprc_9", Pprc_9))
        model.add(MathDetector("Pprc_45", Pprc_45))
        model.add(MathDetector("Pin_carrier", Pin_carrier))
        model.add(MathDetector("Pin_9", Pin_9))
        model.add(MathDetector("Pin_45", Pin_45))
        model.add(MathDetector("Pinx_carrier", Pinx_carrier))
        model.add(MathDetector("Piny_carrier", Piny_carrier))
        model.add(MathDetector("Pas_carrier", abs(Constant(model.a_c0_src)) ** 2))

        model.add(MathDetector("PRG", Pprc_carrier / Pin_carrier))
        model.add(MathDetector("PRG9", Pprc_9 / Pin_9))
        model.add(MathDetector("PRG45", Pprc_45 / Pin_45))
        model.add(MathDetector("AGX", abs(a_carrier_x) ** 2 / Pinx_carrier))
        model.add(MathDetector("AGY", abs(a_carrier_y) ** 2 / Piny_carrier))
        model.add(MathDetector("cost_prcl", Pprc_9 * (Px + Py) / 2))
        # minimize the PRG for the 45 and the carrier through the the OMC for RSE
        model.add(
            MathDetector(
                "cost_srcl",
                Constant(model.PRG45)
                * 1
                / Constant(model.a_u45_00_src)
                * 1
                / Constant(model.a_l45_00_src),
            )
        )

    def add_apertures(self, model):
        if self.options.apertures.test_mass:
            for test_mass in ["ETM", "ITM"]:
                for arm in ["X", "Y"]:
                    self.set_surface_map(
                        model.get(test_mass + arm),
                        maps.aligo_O4_TM_aperture,
                        optic_id=self.params[arm][test_mass].ID,
                        make_axisymmetric=self.options.apertures.make_axisymmetric,
                    )
        # FIXME: might want to separate ESD from total BS to baffle
        if self.options.apertures.BS_ITM:
            self.set_surface_map(model.ITMXlens, maps.aligo_O4_BS_to_ITMX_baffle)
            self.set_surface_map(model.ITMYlens, maps.aligo_O4_BS_to_ITMY_baffle)
        if self.options.apertures.BS_HR:
            self.set_surface_map(model.BS, maps.aligo_O4_BS_HR_baffle)
        if self.options.apertures.PR3_SR3:
            self.set_surface_map(model.PR3, maps.aligo_O4_PR3_SR3_baffle)
            self.set_surface_map(model.SR3, maps.aligo_O4_PR3_SR3_baffle)

    def add_test_mass_thermal(self, model):
        desc_sub = "defocus of substrate lens D=1/f [D / W]"
        desc_srf = "defocus of surface deformations D=2/Rc [D / W]"
        ITM_sub = model.add_parameter("ITM_sub", 300.39e-6, description=desc_sub).ref
        ITM_srf = model.add_parameter("ITM_srf", -46.52e-6, description=desc_srf).ref
        ETM_sub = model.add_parameter("ETM_sub", 209.57e-6, description=desc_sub).ref
        ETM_srf = model.add_parameter("ETM_srf", -33.46e-6, description=desc_srf).ref
        IRH_sub = model.add_parameter("IRH_sub", -9.92e-6, description=desc_sub).ref
        IRH_srf = model.add_parameter("IRH_srf", 0.992e-6, description=desc_srf).ref
        ERH_sub = model.add_parameter("ERH_sub", -12.53e-6, description=desc_sub).ref
        ERH_srf = model.add_parameter("ERH_srf", 0.841e-6, description=desc_srf).ref

        # ring heater powers [W]
        P_RH_ITMX = model.add_parameter(
            "P_RH_ITMX", 0, description="Ring heater power [W]"
        ).ref
        P_RH_ITMY = model.add_parameter(
            "P_RH_ITMY", 0, description="Ring heater power [W]"
        ).ref
        P_RH_ETMX = model.add_parameter(
            "P_RH_ETMX", 0, description="Ring heater power [W]"
        ).ref
        P_RH_ETMY = model.add_parameter(
            "P_RH_ETMY", 0, description="Ring heater power [W]"
        ).ref
        # arm powers [W]
        # FIXME: should this be calculated from the model?
        P_XARM = model.add_parameter("P_XARM", 0e3, description="Arm power [W]").ref
        P_YARM = model.add_parameter("P_YARM", 0e3, description="Arm power [W]").ref
        # coating absorptions
        alpha_ITMX = model.add_parameter(
            "alpha_ITMX", 0.5e-6, description="Coating absorption [fractional]"
        ).ref
        alpha_ITMY = model.add_parameter(
            "alpha_ITMY", 0.4e-6, description="Coating absorption [fractional]"
        ).ref
        alpha_ETMX = model.add_parameter(
            "alpha_ETMX", 0.2e-6, description="Coating absorption [fractional]"
        ).ref
        alpha_ETMY = model.add_parameter(
            "alpha_ETMY", 0.2e-6, description="Coating absorption [fractional]"
        ).ref

        # See https://gitlab.com/ifosim/finesse/finesse3/-/issues/573 for 0+
        defocus_ITMX_lens = (
            1 / model.ITMXlens.f + P_XARM * alpha_ITMX * ITM_sub + P_RH_ITMX * IRH_sub
        )
        defocus_ITMY_lens = (
            1 / model.ITMYlens.f + P_YARM * alpha_ITMY * ITM_sub + P_RH_ITMY * IRH_sub
        )
        # this plus zero is a bug
        # https://gitlab.com/ifosim/finesse/finesse3/-/issues/573
        model.ITMXlens.f = 0 + 1 / defocus_ITMX_lens
        model.ITMYlens.f = 0 + 1 / defocus_ITMY_lens

        defocus_ITMX_Rc = (
            2 / model.ITMX.Rc + P_XARM * alpha_ITMX * ITM_srf + P_RH_ITMX * IRH_srf
        )
        defocus_ITMY_Rc = (
            2 / model.ITMY.Rc + P_YARM * alpha_ITMY * ITM_srf + P_RH_ITMY * IRH_srf
        )
        defocus_ETMX_Rc = (
            2 / model.ETMX.Rc + P_XARM * alpha_ETMX * ETM_srf + P_RH_ETMX * ERH_srf
        )
        defocus_ETMY_Rc = (
            2 / model.ETMY.Rc + P_YARM * alpha_ETMY * ETM_srf + P_RH_ETMY * ERH_srf
        )

        model.ITMX.Rc = 0 + 2 / defocus_ITMX_Rc
        model.ITMY.Rc = 0 + 2 / defocus_ITMY_Rc
        model.ETMX.Rc = 0 + 2 / defocus_ETMX_Rc
        model.ETMY.Rc = 0 + 2 / defocus_ETMY_Rc

    def post_make(self, model, params):
        with finesse.symbols.simplification(allow_flagged=True):
            lx = model.path(self.BS_HR_X.o, self.ITMX_HR_bk.i, symbolic=True)
            ly = model.path(self.BS_HR_Y.o, self.ITMY_HR_bk.i, symbolic=True)

            l_x = model.add(
                fc.Variable("l_x", lx.optical_length, description="BS HR to ITMX HR")
            )
            l_y = model.add(
                fc.Variable("l_y", ly.optical_length, description="BS HR to ITMY HR")
            )
            model.add(
                fc.Variable(
                    "l_schnupp",
                    (l_x - l_y).collect(),
                    description="Schnupp asymmetry (x-y)",
                )
            )

            spx = model.path(self.SRM_HR_fr.o, self.ITMX_HR_bk.i, symbolic=True)
            spy = model.path(self.SRM_HR_fr.o, self.ITMY_HR_bk.i, symbolic=True)
            ppx = model.path(self.PRM_HR_fr.o, self.ITMX_HR_bk.i, symbolic=True)
            ppy = model.path(self.PRM_HR_fr.o, self.ITMY_HR_bk.i, symbolic=True)

            model.add(
                fc.Variable(
                    "L_SRC",
                    ((spx.optical_length + spy.optical_length) / 2).collect(),
                    description="Average optical length of the SRC",
                )
            )
            model.add(
                fc.Variable(
                    "L_PRC",
                    ((ppx.optical_length + ppy.optical_length) / 2).collect(),
                    description="Average optical length of the PRC",
                )
            )


def InitialLock(
    LSC_demod_opt=(
        "CARM",
        "REFL9_I",
        "PRCL",
        "POP9_I",
        "SRCL",
        "POP45_I",
        "DARM",
        "AS45_Q",
    ),
    run_locks=True,
    exception_on_lock_fail=True,
    gain_scale=0.5,
    lock_steps=2000,
    pseudo_lock_arms=True,
):
    """Initial locking action, tries to somewhat replicate the locking prodcedure for
    LSC. This will forcibly use RF readout for DARM as it is easier to initially lock.
    You must use DARM_RF_2_DC action afterwards to go back to DC readout.

    If it can't find a good operating point then the RunLocks step at the end
    will fail. Set :code:`run_locks=False` to skip this or if failing use
    :code:`exception_on_lock_fail=False` to return the locking actions for debugging.

    Parameters
    ----------
    LSC_demod_opt: list, optional
        A pairwise list of degree of freedom and the error signal to use for LSC
        locking.
    run_locks: bool, optional
        Whether to try and run the locks or not.
    exception_on_lock_fail: bool, optional
        Set to false if the locks are failing to debug the locks, see
        :ref:`length_locking`.
    gain_scale: float, optional
        should be a positive scaling factor on how much to scale the optical
        gains. Using 1 can result in unstable behaviour when locking loops are
        cross coupled, hence why it is by default set lower.
    lock_steps: int, optional
        Number of locking steps to try before giving up and failing as a lost
        lock
    pseudo_lock_arms: bool, optional
        When True, the arm cavities are locked by solving for the phasing that
        provides the most HG00 like eigenmode of the arm cavities. When false it
        will try and locally optimise the HG00 power. These can all fail if the
        arm cavity is not close to the true HG00 resonance. Other HGnm resonance
        can also cause scatter into HG00 and make a local maxima.
    """
    if pseudo_lock_arms:
        arm_locks = (
            fa.PseudoLockCavity(
                "cavXARM", lowest_loss=False, feedback="XARM.DC", name="lock XARM"
            ),
            fa.PseudoLockCavity(
                "cavYARM", lowest_loss=False, feedback="YARM.DC", name="lock YARM"
            ),
        )
    else:
        # Maybe could use a bounded global optimisation here
        arm_locks = (
            fa.Maximize("a_carrier_00_x", "XARM.DC"),
            fa.Maximize("a_carrier_00_y", "YARM.DC"),
        )

    action = fa.Series(
        fa.Change(
            {
                "DARM_dc_lock.enabled": False,
                "DARM_rf_lock.enabled": True,
                "PRM.misaligned": True,
                "SRM.misaligned": True,
            },
            name="misalign PRM,SRM",
        ),
        # Lock each arm cavity to the lowest loss mode
        *arm_locks,
        # Put mich on dark fringe
        fa.Minimize("Pas_carrier", "MICH2.DC", name="find dark AS"),
        fa.Noxaxis(name="after ARMs+AS"),
        # Realign the PRM
        fa.Change({"PRM.misaligned": False}, name="align PRM"),
        # get the PRC in roughly the right place whilst keeping arms on resonance
        fa.Maximize("PRG", "PRCL.DC", name="maximise PRG"),
        # get the PRC in roughly the right place whilst keeping arms on resonance
        fa.Maximize("cost_prcl", ["PRCL.DC", "CARM.DC"], name="maxmize Parm*PRG9"),
        fa.Noxaxis(name="after PRC"),
        # Realign SRM
        fa.Change({"SRM.misaligned": False}, name="align SRM"),
        # Try and find signal recycling condition
        fa.Minimize("cost_srcl", "SRCL.DC", name="maximize SRC power"),
        fa.Change({"SRCL.DC": 90}, relative=True),  # add 90 to get to RSE
        fa.Minimize("cost_srcl", "SRCL.DC", name="minimize PRG45"),
        fa.Noxaxis(name="after SRC"),
        fa.OptimiseRFReadoutPhaseDC(*LSC_demod_opt),
        fa.SetLockGains(d_dof_gain=1e-10, gain_scale=gain_scale),
        fa.RunLocks(
            max_iterations=lock_steps, exception_on_fail=exception_on_lock_fail
        ),
    )
    if not run_locks:
        action.actions = action.actions[:-1]
    return action


class DARM_RF_to_DC(fa.Action):
    def __init__(
        self,
        offset=0.5e-3,
        exception_on_lock_fail=True,
        lock_steps=1000,
        name="DarmRF2DC",
    ):
        """Locks a model using DARM RF readout then transitions the model into using a
        DC readout and locks. This will run an RF lock first, apply some small offsets,
        then run the DC locks.

        Parameters
        ----------
        offset : float
            Initial offset to apply to DARM to find the DC readout lock point.
            Can use this to change which side of DARM error to use.
        """

        super().__init__(name)
        self.offset = offset

        self.__lock_rf = fa.RunLocks(
            "DARM_rf_lock",
            max_iterations=lock_steps,
            exception_on_fail=exception_on_lock_fail,
            name="run RF lock",
        )
        self.__lock_dc = fa.RunLocks(
            "DARM_dc_lock",
            max_iterations=lock_steps,
            exception_on_fail=exception_on_lock_fail,
            name="run DC lock",
        )

    def _do(self, state):
        self.__lock_rf._do(state)

        state.model.DARM_rf_lock.enabled = False
        # kick lock away from zero tuning for DC lock to grab with
        state.model.DARM.DC += self.offset
        # take a guess at the gain
        state.model.DARM_dc_lock.gain = -0.01
        state.model.DARM_dc_lock.enabled = True
        self.__lock_dc._do(state)
        return None

    def _requests(self, model, memo, first=True):
        memo["changing_parameters"].append("DARM_dc_lock.gain")
        memo["changing_parameters"].append("DARM_dc_lock.enabled")
        memo["changing_parameters"].append("DARM_rf_lock.enabled")
        self.__lock_rf._requests(model, memo)
        self.__lock_dc._requests(model, memo)
        return memo
