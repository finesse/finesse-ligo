import numpy as np
from abc import ABC, abstractmethod
import importlib
import os

from more_itertools import roundrobin
from munch import Munch  # pip install Munch
import finesse
import finesse_ligo
import finesse.components as fc
from finesse.materials import FusedSilica
from finesse.detectors import (
    PowerDetector,
    AmplitudeDetector,
    MathDetector,
    QuantumNoiseDetector,
)
from finesse.components.general import LocalDegreeOfFreedom
from finesse.components import Mirror
from finesse.symbols import Constant
from finesse.knm import Map

from .. import maps


def update_dict(original, p):
    """Updates a dictionary with some new keys and values. This happens recursively, so
    any dicts that are also present will also be updated.

    Parameters
    ----------
    original : dict
        Original to update
    p : dict
        Parameters to update `original` with
    """
    for k, v in p.items():
        if isinstance(v, dict):
            if k in original:
                update_dict(original[k], v)
            else:
                original[k] = v
        else:
            original[k] = v


class FinesseFactory(ABC):
    def __init__(self, parameters):
        self.params = Munch()
        self.update_parameters(parameters)
        self.reset()

    def update_options(self, options):
        """Update the options of this factory.

        Parameters
        ----------
        options : tuple | list | Dict | Munch
            A set of options values to update, can be a list/tuple of option-value pairs
            or another Munch dictionary. Dictionary inputs will be munchified see `Munch.munchify`

        Examples
        --------
        Using option-value pairs in a list or tuple:
        >>> factory.update_options(
        ...     [
        ...         ("ASC.add", True),
        ...         ("LSC.add", True),
        ...     ]
        ... )

        Using a dictionary:
        >>> factory.update_options(
        ...     {
        ...         "ASC":{"add":True},
        ...         "LSC":{"add":True},
        ...     }
        ... )
        """
        from munch import munchify

        if isinstance(options, (list, tuple)):
            for key, value in options:
                key = key.split(".")
                option = self.options
                for k in key[:-1]:
                    option = option[k]
                option[key[-1]] = value

        elif isinstance(options, Munch):
            update_dict(self.options, options)
        elif isinstance(options, dict):
            self.update_options(munchify(options))
        else:
            raise ValueError(
                "Input options should be a list/tuple of (key,values) or a Munch dictionary"
            )

    def update_parameters(self, parameters):
        """Update the parameters.

        Parameters
        ----------
        parameters : MunchDict | str
            Munch dict of parameters, or a filename and path
        """
        if isinstance(parameters, Munch):
            update_dict(self.params, parameters)

        elif os.path.exists(parameters):
            with open(parameters) as f:
                update_dict(self.params, Munch.fromYAML(f.read()))

        elif importlib.resources.is_resource(
            "finesse_ligo.parameter_files", parameters
        ):
            update_dict(
                self.params,
                Munch.fromYAML(
                    importlib.resources.read_text(
                        "finesse_ligo.parameter_files", parameters
                    )
                ),
            )
        else:
            raise Exception(f"Could not handle parameters: {parameters}")

    @abstractmethod
    def reset(self):
        pass

    @abstractmethod
    def make(self):
        pass

    def _add_all_amplitude_detectors(
        self, model, port, key, *freqs, add_power_detectors=False
    ):
        """Add amplitude detectors for the carrier and specified sidebands to a node.

        FIXME: this should allow specification of carrier HOMs as well, but that doesn't easily
        comport with the current naming conventions used in the existing aLIGO model

        Parameters
        ----------
        model : :class:`finesse.Model`
            Model to add the detectors to
        port : str, :class:`Node`
            Node to probe
        key : str
            kdkd
        *freqs : list of (frequency reference, str) tuples or (frequency reference, str, int, int)
        tuples, optional
            If a tuple (f, key), list of frequencies to probe along with the detector name to add
            to the model. Both upper and lower sidebands are probed.
            If a tuple (f, key, n, m), list of frequencies and HOMs to probe along wit the detector
            name to add to the model
        add_power_detectors : bool, optional; default: False
            If True, adds a detector for the power of each amplituded detector added to the model

        Examples
        --------
        To probe the carrier and 9 and 45 MHz sidebands
        >>> freqs = [(9, "9"), (45, "45")]
        >>> _add_all_amplitude_detectors(model, model.PRM.p1.o, "PRC", *freqs)
        adds the amplitude detectors a_c0_PRC probing the carrier power in the PRC and
        a_u9_PRC, a_l9_PRC, a_u45_PRC, and a_l45_PRC probing the upper and lower 9 and 45 MHz
        sidebands in the PRC. If called with add_power_detectors=True, the detectors P_c0_PRC,
        P_u9_PRC, P_l9_PRC, ... would also be added probing the powers of those fields.

        To probe the carrier 9 and 45 MHz sidebands as well as the 45 MHz 00, 01, and 10 using
        as symbolic reference to frequencies
        >>> f1 = model.f1.ref
        >>> f2 = model.f2.ref
        >>> freqs = [
            (f1, "9"),
            (f2, "45"),
            (f2, "45", 0, 0),
            (f2, "45", 1, 0),
            (f2, "45", 0, 1),
        ]
        >>> _add_all_amplitude_detectors(model, model.PRM.p1.o, "PRC", *freqs)
        in addition to the above, a_u45_00_PRC, a_u45_10_PRC, a_l45_10_PRC, etc. will be added
        """
        AD_c = Constant(model.add(AmplitudeDetector(f"a_c0_{key}", port, f=0)))
        if add_power_detectors:
            model.add(MathDetector(f"P_c0_{key}", np.abs(AD_c) ** 2))
        for det_options in freqs:
            if len(det_options) == 2:
                f, sb_key = det_options
                nm = dict(n=None, m=None)
                postfix = f"{sb_key}_{key}"
            elif len(det_options) == 4:
                f, sb_key, n, m = det_options
                nm = dict(n=n, m=m)
                postfix = f"{sb_key}_{n}{m}_{key}"
            else:
                raise finesse.exceptions.FinesseException(
                    "Incorrect number of detector options"
                )
            AD_u = Constant(
                model.add(AmplitudeDetector(f"a_u{postfix}", port, f=+f, **nm))
            )
            AD_l = Constant(
                model.add(AmplitudeDetector(f"a_l{postfix}", port, f=-f, **nm))
            )
            if add_power_detectors:
                model.add(MathDetector(f"P_u{postfix}", np.abs(AD_u) ** 2))
                model.add(MathDetector(f"P_l{postfix}", np.abs(AD_l) ** 2))

    def _mirror_surface_normals_parallel(self, comp1, comp2):
        """Check if the normals of the surfaces of two components are aligned.

        Parameters
        ----------
        comp1, comp2 : Mirror
            Components to check

        Returns
        -------
        bool
            True if the normals are aligned, False otherwise
        """
        return (
            comp1.p1.attached_to is comp2.p2.attached_to
            or comp2.p1.attached_to is comp1.p2.attached_to
        )

    def _beamsplitter_surface_normals_parallel(self, comp1, comp2):
        """Check if the normals of the surfaces of two beamsplitters are aligned.

        Parameters
        ----------
        comp1, comp2 : Beampslitter
            Components to check

        Returns
        -------
        bool
            True if the normals are aligned, False otherwise
        """
        return (
            comp1.p1.attached_to is comp2.p3.attached_to
            or comp1.p1.attached_to is comp2.p4.attached_to
            or comp2.p1.attached_to is comp1.p3.attached_to
            or comp2.p1.attached_to is comp1.p4.attached_to
        )

    def _link_all_mechanical_dofs(self, comp1, comp2):
        """Connects signal flow for the mechanical motion of component 1 to component 2.
        Doesn't create a bi-directional connection.

        Parameters
        ----------
        comp1, comp2 : ModelElement
            Components to connect
        """
        if comp1._model is not comp2._model:
            raise RuntimeError(f"{comp1} and {comp2} are not part of the same model")

        model = comp1._model

        # Are the surface normals parallel or anti-parallel?
        if isinstance(comp1, fc.Mirror) and isinstance(comp2, fc.Mirror):
            parallel = self._mirror_surface_normals_parallel(comp1, comp2)
        elif isinstance(comp1, fc.Beamsplitter) and isinstance(comp2, fc.Beamsplitter):
            parallel = self._beamsplitter_surface_normals_parallel(comp1, comp2)
        else:
            raise NotImplementedError()

        if not parallel:
            z_gain = -1
            # Pitch sign flips when surfaces pointing towards each other
            pitch_gain = -1
        elif (comp1.p1.attached_to is comp2.p2.attached_to) or (
            comp1.p2.attached_to is comp2.p1.attached_to
        ):
            z_gain = +1
            pitch_gain = +1
        else:
            raise NotImplementedError()

        model.connect(comp1.mech.z, comp2.mech.z, gain=z_gain)
        model.connect(comp1.mech.pitch, comp2.mech.pitch, gain=pitch_gain)
        model.connect(comp1.mech.yaw, comp2.mech.yaw)  # yaw_gain always == 1


class DRFPMIFactory(FinesseFactory):
    def set_default_options(self):
        """Resets DRFPMI specific default options."""
        self.options = Munch()
        self.options.BS_type = "thick"
        self.options.add_9MHz = True
        self.options.add_45MHz = True
        self.options.add_118MHz = False
        self.options.add_detectors = True
        self.options.QUAD_suspension_model = None
        self.options.QUAD_suspension_kwargs = {}
        self.options.add_transmon = False
        self.options.fake_src_gouy = False
        self.options.fake_prc_gouy = False
        self.options.BS_trans_arm = "X"
        self.options.PR2_PR3_substrates = False

        self.options.INPUT = Munch()
        self.options.INPUT.add_IMC_and_IM1 = False

        self.options.LSC = Munch()
        self.options.LSC.add_DOFs = True
        self.options.LSC.add_readouts = True
        self.options.LSC.add_output_detectors = True
        self.options.LSC.add_locks = True
        self.options.LSC.add_AC_loops = True
        self.options.LSC.close_AC_loops = False

        self.options.materials = Munch()
        self.options.materials.test_mass_substrate = FusedSilica

    def add_arm_cavity(self, model, which, params, options):
        # Put all loss on the ETM so it separates out arm losses and SRC/PRC loss
        # when you add it to the ITM
        which = which.upper()
        arm_loss = model.add_parameter(f"{which}_arm_loss", params.arm_loss).ref

        ITM = model.add(fc.Mirror(f"ITM{which}", T=params.ITM.T, L=0, Rc=params.ITM.Rc))

        ETM = model.add(
            fc.Mirror(f"ETM{which}", T=params.ETM.T, L=arm_loss, Rc=params.ETM.Rc)
        )

        ITMAR = model.add(
            fc.Mirror(
                f"ITM{which}AR",
                T=1,
                L=0,
                xbeta=ITM.xbeta.ref,
                ybeta=ITM.ybeta.ref,
                phi=ITM.phi.ref,
            )
        )

        model.connect(ITM.p1, ETM.p1, name=f"L{which}", L=params.length_arm)
        model.connect(
            ITMAR.p1,
            ITM.p2,
            name=f"subITM{which}",
            L=params.ITM.thickness,
            nr=self.options.materials.test_mass_substrate.nr,
        )
        self._link_all_mechanical_dofs(ITM, ITMAR)

        if options.add_detectors:
            model.add(PowerDetector(f"P{which.lower()}", ETM.p1.o))
            model.add(PowerDetector(f"Pin{which.lower()}", ITMAR.p2.i))
            model.add(
                AmplitudeDetector(
                    f"a_carrier_pin{which.lower()}",
                    ITM.p2.i,
                    f=0,
                )
            )
            model.add(
                AmplitudeDetector(
                    f"a_carrier_{which.lower()}",
                    ETM.p1.o,
                    f=0,
                )
            )
            model.add(
                AmplitudeDetector(
                    f"a_carrier_00_{which.lower()}",
                    ETM.p1.o,
                    f=0,
                    n=0,
                    m=0,
                )
            )

        setattr(self, f"ITM{which}_AR_fr", ITMAR.p2)
        setattr(self, f"ITM{which}_HR_bk", ITM.p2)

    def add_PRC(self, model, params, BS_port, options):
        PRM = model.add(
            fc.Mirror("PRM", T=params.PRM.T, L=params.PRM.L, Rc=params.PRM.Rc)
        )

        if hasattr(params.PRMAR, "T"):
            PRMAR_T = float(params.PRMAR.T)
        else:
            PRMAR_T = 1.0 - float(params.PRMAR.R) - float(params.PRMAR.L)

        PRMAR = model.add(
            fc.Mirror(
                "PRMAR",
                T=PRMAR_T,
                L=params.PRMAR.L,
                Rc=params.PRMAR.Rc,
                xbeta=PRM.xbeta.ref,
                ybeta=PRM.ybeta.ref,
                phi=PRM.phi.ref,
            )
        )

        PR2 = model.add(
            fc.Beamsplitter(
                "PR2",
                T=params.PR2.T,
                L=params.PR2.L,
                Rc=params.PR2.Rc,
                alpha=params.PR2.AOI,
            )
        )

        PR3 = model.add(
            fc.Beamsplitter(
                "PR3",
                T=params.PR3.T,
                L=params.PR3.L,
                Rc=params.PR3.Rc,
                alpha=params.PR3.AOI,
            )
        )

        model.connect(PRM.p1, PR2.p1, name="lp1", L=params.length_PRM_PR2)
        model.connect(PR2.p2, PR3.p1, name="lp2", L=params.length_PR2_PR3)
        model.connect(PR3.p2, BS_port, name="lp3", L=params.length_PR3_BS)

        if self.options.get("fake_prc_gouy", False):
            prc_gouy = model.add_parameter("PRC_gouy", 20)
            prc_gouy_astig = model.add_parameter("PRC_gouy_astig", 0)
            model.lp1.user_gouy_x = prc_gouy.ref + prc_gouy_astig.ref
            model.lp1.user_gouy_y = prc_gouy.ref - prc_gouy_astig.ref
            model.lp1.user_gouy_y = model.lp2.user_gouy_x = 0
            model.lp1.user_gouy_y = model.lp3.user_gouy_x = 0

        model.connect(
            PRMAR.p1, PRM.p2, name="subPRM", L=params.PRM.thickness, nr=FusedSilica.nr
        )
        self._link_all_mechanical_dofs(PRM, PRMAR)

        self.POP_port = PR2.p3
        self.PRM_AR_fr = PRMAR.p2
        self.PRM_HR_fr = PRM.p1

        if options.PR2_PR3_substrates:
            wedge = params.PR2AR.get("wedge", 0)
            PR2AR = model.add(
                fc.Mirror(
                    "PR2AR",
                    R=params.PR2AR.R,
                    L=params.PR2AR.L,
                    Rc=params.PR2AR.Rc,
                    xbeta=PR2.xbeta.ref + wedge,
                    ybeta=PR2.ybeta.ref,
                    phi=PR2.phi.ref,
                )
            )
            self._link_all_mechanical_dofs(PR2, PR2AR)
            model.connect(
                PR2.p3,
                PR2AR.p1,
                name="subPR2",
                L=params.PR2.thickness,
                nr=FusedSilica.nr,
            )

            PR3AR = model.add(
                fc.Mirror(
                    "PR3AR",
                    R=params.PR3AR.R,
                    L=params.PR3AR.L,
                    Rc=params.PR3AR.Rc,
                    xbeta=PR3.xbeta.ref + params.PR3AR.wedge,
                    ybeta=PR3.ybeta.ref,
                    phi=PR3.phi.ref,
                )
            )
            self._link_all_mechanical_dofs(PR3, PR3AR)
            model.connect(
                PR3.p3,
                PR3AR.p1,
                name="subPR3",
                L=params.PR3.thickness,
                nr=FusedSilica.nr,
            )

        if options.add_detectors:
            model.add(PowerDetector("Pprc", model.PRM.p1.o))
            model.add(AmplitudeDetector("a_carrier_in", self.PRM_AR_fr.i, f=0))
            model.add(AmplitudeDetector("a_carrier_prc", model.PRM.p1.o, f=0))

    def add_SRC(self, model, params, BS_port, options):
        SRM = model.add(
            fc.Mirror("SRM", T=params.SRM.T, L=params.SRM.L, Rc=params.SRM.Rc)
        )

        SRMAR = model.add(
            fc.Mirror(
                "SRMAR",
                T=params.SRMAR.T,
                L=params.SRMAR.L,
                Rc=params.SRMAR.Rc,
                xbeta=SRM.xbeta.ref,
                ybeta=SRM.ybeta.ref,
                phi=SRM.phi.ref,
            )
        )

        SR2 = model.add(
            fc.Beamsplitter(
                "SR2",
                T=params.SR2.T,
                L=params.SR2.L,
                Rc=params.SR2.Rc,
                alpha=params.SR2.AOI,
            )
        )

        SR3 = model.add(
            fc.Beamsplitter(
                "SR3",
                T=params.SR3.T,
                L=params.SR3.L,
                Rc=params.SR3.Rc,
                alpha=params.SR3.AOI,
            )
        )

        model.connect(
            SRMAR.p1, SRM.p2, name="subSRM", L=params.SRM.thickness, nr=FusedSilica.nr
        )
        model.connect(SRM.p1, SR2.p1, name="ls1", L=params.length_SRM_SR2)
        model.connect(SR2.p2, SR3.p1, name="ls2", L=params.length_SR2_SR3)
        model.connect(SR3.p2, BS_port, name="ls3", L=params.length_SR3_BS)

        if self.options.get("fake_src_gouy", False):
            src_gouy = model.add_parameter("SRC_gouy", 20)
            src_gouy_astig = model.add_parameter("SRC_gouy_astig", 0)
            model.ls1.user_gouy_x = src_gouy.ref - src_gouy_astig.ref
            model.ls1.user_gouy_y = src_gouy.ref + src_gouy_astig.ref
            model.ls2.user_gouy_y = model.ls2.user_gouy_x = 0
            model.ls3.user_gouy_y = model.ls3.user_gouy_x = 0

        self._link_all_mechanical_dofs(SRM, SRMAR)

        if self.options.get("BS_type", "thick") in ["thin", "fake"]:
            # Adds extra optical path length to get correct SRC length if
            # using a thin BS
            _, L_BS_sub = self._BS_substrate_length(model.BS, self.params.BS)
            model.ls3.L += L_BS_sub * FusedSilica.nr

        self.SRM_AR_fr = SRMAR.p2
        self.SRM_HR_fr = SRM.p1

        if options.add_detectors:
            model.add(PowerDetector("Psrc", model.SRM.p1.o))
            model.add(AmplitudeDetector("a_carrier_src", model.SRM.p1.i, f=0))

    def _BS_substrate_length(self, BS, params_BS):
        # Use snells law to compute the angle the ray makes with
        # the normal to the HR surface, since the the glass
        # this is alpha_AR
        alpha_AR = np.arcsin(1 / FusedSilica.nr * np.sin(np.deg2rad(BS.alpha.ref)))

        # Neglecting any wedge on the BS
        # the apparent thickness is obtained from trig.
        L_BS_sub = params_BS.thickness / np.cos(alpha_AR)
        return alpha_AR, L_BS_sub

    def add_BS(self, model, params):
        BS = model.add(
            fc.Beamsplitter(
                "BS",
                T=params.T,
                L=params.L,
                alpha=params.AOI,
            )
        )

        BS_type = self.options.get("BS_type", "thick")
        if BS_type not in ["thick", "thin", "fake"]:
            raise ValueError(f"Unrecognized BS type: {BS_type}")

        self.options.BS_trans_arm = self.options.BS_trans_arm.upper()
        assert self.options.BS_trans_arm in ("X", "Y")
        trans_arm = self.options.BS_trans_arm

        if BS_type == "fake":
            if trans_arm == "X":
                self.BS_X = BS.p3
                self.BS_Y = BS.p2
            else:
                self.BS_X = BS.p2
                self.BS_Y = BS.p3
            self.BS_PR = BS.p1
            self.BS_SR = BS.p4
            self.BS_HR_X = BS.p3
            self.BS_HR_Y = BS.p2
        else:
            alpha_AR, L_BS_sub = self._BS_substrate_length(BS, params)
            if BS_type == "thin":
                L_BS_sub = 0
            BSARarm = model.add(
                fc.Beamsplitter(
                    f"BSAR{trans_arm}",
                    R=0,
                    L=params.R_AR,
                    alpha=np.rad2deg(alpha_AR),
                    phi=BS.phi.ref,
                    xbeta=BS.xbeta.ref,
                    ybeta=BS.ybeta.ref,
                )
            )
            BSARSR = model.add(
                fc.Beamsplitter(
                    "BSARAS",
                    R=0,
                    L=params.R_AR,
                    alpha=np.rad2deg(alpha_AR),
                    phi=BS.phi.ref,
                    xbeta=BS.xbeta.ref,
                    ybeta=BS.ybeta.ref,
                )
            )
            model.connect(
                BS.p3,
                BSARarm.p1,
                L=L_BS_sub,
                name=f"subBS_{trans_arm}",
                nr=FusedSilica.nr,
            )
            model.connect(
                BS.p4,
                BSARSR.p2,
                L=L_BS_sub,
                name="subBS_SR",
                nr=FusedSilica.nr,
            )
            self._link_all_mechanical_dofs(BS, BSARarm)
            self._link_all_mechanical_dofs(BS, BSARSR)

            if trans_arm == "X":
                self.BS_X = BSARarm.p3
                self.BS_Y = BS.p2
                self.BS_HR_X = BS.p3
                self.BS_HR_Y = BS.p2
            else:
                self.BS_X = BS.p2
                self.BS_Y = BSARarm.p3
                self.BS_HR_X = BS.p2
                self.BS_HR_Y = BS.p3

            self.BS_PR = BS.p1
            self.BS_SR = BSARSR.p4

    def add_MICH(self, model, which, params, BS_port, ITM_AR_fr_port):
        # Conceptually we are setting up
        #          |                    CP1  CP2         ITM_AR_fr_port
        #          |                     v    v                v
        # Laser -- / - length_BS_CP - ( )|-FS-| -length_CP_TM- |-FS-(
        #          ^                   ^                         ^
        #          BS                ITMlens                    ITM
        # Where:
        #  -FS- indicates a substrate
        #  | is a flat mirror surface
        #  ( ) is a lens
        #  ( is a concave surface
        # In other words, the "lens" is attached with a lenght 0 space
        # to the compensation plateand the combined curvature of the
        # test mass and compensation plate is applied at the CP.
        # This assumes negligable Gouy phase accumulation over the space
        # length_CP_TM
        ITMlens = model.add(
            fc.Lens(
                f"ITM{which}lens",
                f=1
                / (1 / params.CP.cold_focal_length + 1 / params.ITM.cold_focal_length),
            )
        )

        CP1 = model.add(fc.Mirror(f"CP{which}1", T=1, L=0))
        CP2 = model.add(fc.Mirror(f"CP{which}2", T=1, L=0))

        model.connect(
            BS_port, ITMlens.p1, name=f"l{which.lower()}1", L=params.length_BS_CP
        )

        trans_arm = self.options.BS_trans_arm
        use_thin_BS = self.options.get("BS_type", "thick") in ["thin", "fake"]
        if use_thin_BS and which.upper() == trans_arm:
            # If we are using a thin BS then we need to take into account the lost path
            # length from using a thin BS to keep the same schnupp length
            _, L_BS_sub = self._BS_substrate_length(model.BS, self.params.BS)
            model.get(f"l{which.lower()}1").L += L_BS_sub * FusedSilica.nr

        model.connect(ITMlens.p2, CP1.p1, name=f"l{which.lower()}2", L=0)
        model.connect(
            CP1.p2,
            CP2.p2,
            name=f"subCP{which}",
            L=params.CP.thickness,
            nr=FusedSilica.nr,
        )
        model.connect(
            CP2.p1, ITM_AR_fr_port, name=f"l{which.lower()}3", L=params.length_CP_TM
        )

    def add_IMC(self, model, IMC):
        MC1 = model.add(
            fc.Beamsplitter(
                "MC1",
                T=IMC.MC1.T,
                L=IMC.MC1.L,
                Rc=IMC.MC1.Rc,
                alpha=IMC.MC1.AOI,
            )
        )
        MC2 = model.add(
            fc.Beamsplitter(
                "MC2",
                T=IMC.MC2.T,
                L=IMC.MC2.L,
                Rc=IMC.MC2.Rc,
                alpha=IMC.MC2.AOI,
            )
        )
        MC3 = model.add(
            fc.Beamsplitter(
                "MC3",
                T=IMC.MC3.T,
                L=IMC.MC3.L,
                Rc=IMC.MC3.Rc,
                alpha=IMC.MC3.AOI,
            )
        )
        model.connect(MC1.p2, MC2.p1, L=IMC.length_MC1_MC2)
        model.connect(MC2.p2, MC3.p1, L=IMC.length_MC2_MC3)
        model.connect(MC3.p2, MC1.p1, L=IMC.length_MC3_MC1)
        model.add(fc.Cavity("cavIMC", MC3.p1.o))

        # Connect lasers/modulators to MC1.p3 which injects into the
        # IMC into the long path towards MC2
        return MC1.p4, MC3.p3  # input and output port

    def add_input_path(self, model, INPUT, PRM_AR_port):
        LASER = model.add(fc.Laser("L0", P=INPUT.LASER.power))

        f1 = model.add_parameter("f1", INPUT.LASER.f1).ref
        f2 = model.add_parameter("f2", 5 * f1).ref
        f3 = model.add_parameter("f3", 13 * f1).ref
        modulators = []

        if self.options.add_9MHz:
            modulators.append(
                model.add(fc.Modulator("mod1", midx=INPUT.LASER.mod_depth_9, f=f1))
            )

        if self.options.add_45MHz:
            modulators.append(
                model.add(fc.Modulator("mod2", midx=INPUT.LASER.mod_depth_45, f=f2))
            )

        if self.options.add_118MHz:
            modulators.append(
                model.add(fc.Modulator("mod3", midx=INPUT.LASER.mod_depth_118, f=f3))
            )

        if len(modulators) == 0:
            LASER_port_out = LASER.p1
        else:
            model.link(LASER, *modulators)
            LASER_port_out = modulators[-1].p2

        IFI = model.add(fc.DirectionalBeamsplitter("IFI"))
        IM2 = model.add(
            fc.Beamsplitter(
                "IM2",
                T=INPUT.IM2.T,
                L=INPUT.IM2.L,
                Rc=INPUT.IM2.Rc,
                alpha=INPUT.IM2.AOI,
            )
        )
        IM3 = model.add(
            fc.Beamsplitter(
                "IM3",
                T=INPUT.IM3.T,
                L=INPUT.IM3.L,
                Rc=INPUT.IM3.Rc,
                alpha=INPUT.IM3.AOI,
            )
        )
        IM4 = model.add(
            fc.Beamsplitter(
                "IM4",
                T=INPUT.IM4.T,
                L=INPUT.IM4.L,
                Rc=INPUT.IM4.Rc,
                alpha=INPUT.IM4.AOI,
            )
        )

        model.connect(IM2.p2, IFI.p1, L=INPUT.length_IM2_IFI)
        model.connect(IFI.p3, IM3.p1, L=INPUT.length_IFI_IM3)
        model.connect(IM3.p2, IM4.p1, L=INPUT.length_IM3_IM4)
        model.connect(IM4.p2, PRM_AR_port, L=INPUT.length_IM4_PRM_AR)

        self.add_REFL_telescope(model)

        self.REFL_port = model.M5.p2  # IFI.p4

        if not self.options.INPUT.add_IMC_and_IM1:
            try:
                from finesse.tracing.abcd import space, beamsplitter_refl

                beamsplitter_refl_t = lambda *args: beamsplitter_refl(*args, "x")
                beamsplitter_refl_s = lambda *args: beamsplitter_refl(*args, "y")
            except ImportError:
                # Handle newer versions which separate the beamsplitter refl
                # into the tangential and sagittal planes from 3.0a28
                from finesse.tracing.abcd import (
                    space,
                    beamsplitter_refl_s,
                    beamsplitter_refl_t,
                )

            # don't include IMC so connect straight through to IM2
            model.connect(LASER_port_out, IM2.p1)
            # Need to compute IMC mode for the input beam, add it to a
            # temporary model, compute the eigenmodes and extract it
            tmp = finesse.Model()
            IMC_input_port, IMC_output_port = self.add_IMC(tmp, INPUT.IMC)
            tmp.beam_trace()
            # Get the q going out of the output port
            qx = IMC_output_port.o.qx
            qy = IMC_output_port.o.qy
            # Make ABCDs to go from from IMC to IM2
            LIMC_IM1 = space(INPUT.length_IMC_IM1)
            nr = 1  # vacuum
            RIM1_X = beamsplitter_refl_t(INPUT.IM1.Rc, INPUT.IM1.AOI, nr)
            RIM1_Y = beamsplitter_refl_s(INPUT.IM1.Rc, INPUT.IM1.AOI, nr)
            LIM1_IM2 = space(INPUT.length_IM1_IM2)
            # Propagate from IMC to IM2
            IMC_IM2_X = LIM1_IM2 @ RIM1_X @ LIMC_IM1
            IMC_IM2_Y = LIM1_IM2 @ RIM1_Y @ LIMC_IM1
            # Set input mode going into IM2
            model.IM2.p1.i.qx = qx.transform(IMC_IM2_X)
            model.IM2.p1.i.qy = qy.transform(IMC_IM2_Y)
        else:
            # Add the IMC To this model and connect up IM4
            IMC_input_port, IMC_output_port = self.add_IMC(model, INPUT.IMC)

            IM1 = model.add(
                fc.Beamsplitter(
                    "IM1",
                    T=INPUT.IM1.T,
                    L=INPUT.IM1.L,
                    Rc=INPUT.IM1.Rc,
                    alpha=INPUT.IM1.AOI,
                )
            )

            model.connect(LASER_port_out, IMC_input_port)
            model.connect(IMC_output_port, IM1.p1, L=INPUT.length_IMC_IM1)
            model.connect(IM1.p2, IM2.p1, L=INPUT.length_IM1_IM2)

    def add_REFL_telescope(self, model):
        par = self.params.INPUT.Telescope
        # make a second IM2
        model.add(fc.Beamsplitter("IM2_REFL", **par.IM2_REFL))

        # set up mirrors on HAM1
        model.add(fc.Beamsplitter("LossyMirror", **par.LossyMirror))
        model.add(fc.Beamsplitter("RM1", **par.RM1))
        model.add(fc.Beamsplitter("RM2", **par.RM2))
        model.add(fc.Beamsplitter("M5", **par.M5))
        model.add(fc.Beamsplitter("M6", **par.M6))
        model.add(fc.Lens("REFL_L101", **par.REFL_L101))
        model.add(fc.Lens("REFL_L102", **par.REFL_L102))
        model.add(fc.Beamsplitter("WFS_REFL_BS", **par.WFS_REFL_BS))
        model.add(fc.Beamsplitter("LSC_REFL_BS", **par.LSC_REFL_BS))

        # create placeholder for LSC RFPDs and ASC WFS
        model.add(fc.Nothing("ASC_REFL_A"))
        model.add(fc.Nothing("ASC_REFL_B"))
        model.add(fc.Nothing("LSC_REFL_A"))
        model.add(fc.Nothing("LSC_REFL_B"))

        def L(k1, k2):
            return par.get(f"length_{k1}_{k2}")

        # connect HAM2 to HAM1
        model.connect(model.IFI.p4, model.IM2_REFL.p1, L=L("IFI", "IM2_REFL"))
        model.connect(
            model.IM2_REFL.p2, model.LossyMirror.p1, L=L("IM2_REFL", "LossyMirror")
        )
        model.connect(model.LossyMirror.p3, model.RM1.p1, L=L("LossyMirror", "RM1"))
        model.connect(model.RM1.p2, model.RM2.p1, L=L("RM1", "RM2"))
        model.connect(model.RM2.p2, model.M5.p1, L=L("RM2", "M5"))
        model.connect(model.M5.p2, model.M6.p1, L=L("M5", "M6"))
        model.connect(model.M6.p3, model.REFL_L101.p1, L=L("M6", "L101"))
        model.connect(model.REFL_L101.p2, model.REFL_L102.p1, L=L("L101", "L102"))
        model.connect(model.REFL_L102.p2, model.WFS_REFL_BS.p1, L=L("L102", "WFS_BS"))

        # connect to LSC RFPDs
        model.connect(model.M6.p2, model.LSC_REFL_BS.p1)
        model.connect(model.LSC_REFL_BS.p3, model.LSC_REFL_A.p1)
        model.connect(model.LSC_REFL_BS.p2, model.LSC_REFL_B.p1)

        # connect to WFS
        model.connect(model.WFS_REFL_BS.p3, model.ASC_REFL_A.p1, L=L("WFS_BS", "ASC_A"))
        model.connect(model.WFS_REFL_BS.p2, model.ASC_REFL_B.p1, L=L("WFS_BS", "ASC_B"))

    def add_AS_telescopes(self, model):
        parAB = self.params.OUTPUT.Telescopes.AS_A_B
        parC = self.params.OUTPUT.Telescopes.AS_C
        # lens in transmission of OM1
        model.add(fc.Lens("AS_L1", **parC.AS_L1))

        # lens in transmission of OM3
        model.add(fc.Lens("AS_L101", **parAB.AS_L101))

        # add in BS between AS A and AS B WFS
        model.add(fc.Beamsplitter("AS_M101", **parAB.AS_M101))

        # add in BS between OM1 and AS_C lens
        model.add(fc.Beamsplitter("AS_M6", **parC.AS_M6))

        # set up nothing at AS A and B to put WFS, C for QPD
        model.add(fc.Nothing("AS_A"))
        model.add(fc.Nothing("AS_B"))
        model.add(fc.Nothing("AS_C"))

        model.connect(model.OM3.p3, model.AS_L101.p1, L=parAB.length_OM3_L101)
        model.connect(model.AS_L101.p2, model.AS_M101.p1, L=parAB.length_L010_M101)
        model.connect(model.AS_M101.p2, model.AS_A.p1, L=parAB.length_M101_AS_A)
        model.connect(model.AS_M101.p3, model.AS_B.p1, L=parAB.length_M101_AS_B)
        model.connect(model.OM1.p3, model.AS_M6.p1, L=parC.length_OM1_M6)
        model.connect(model.AS_M6.p3, model.AS_L1.p1, L=parC.length_M6_L1)
        model.connect(model.AS_L1.p2, model.AS_C.p1, L=parC.length_L1_AS_C)

    def add_output_path(self, model, OUTPUT, SRM_AR_fr, OMC_input_port):
        OFI = model.add(fc.DirectionalBeamsplitter("OFI"))
        OM1 = model.add(
            fc.Beamsplitter(
                "OM1",
                T=OUTPUT.OM1.T,
                L=OUTPUT.OM1.L,
                Rc=OUTPUT.OM1.Rc,
                alpha=OUTPUT.OM1.AOI,
            )
        )
        OM2 = model.add(
            fc.Beamsplitter(
                "OM2",
                T=OUTPUT.OM2.T,
                L=OUTPUT.OM2.L,
                Rc=OUTPUT.OM2.Rc,
                alpha=OUTPUT.OM2.AOI,
            )
        )
        OM3 = model.add(
            fc.Beamsplitter(
                "OM3",
                T=OUTPUT.OM3.T,
                L=OUTPUT.OM3.L,
                Rc=OUTPUT.OM3.Rc,
                alpha=OUTPUT.OM3.AOI,
            )
        )
        model.connect(SRM_AR_fr, OFI.p1, L=OUTPUT.length_SRM_OFI)
        model.connect(OFI.p3, OM1.p1, L=OUTPUT.length_OFI_OM1)
        model.connect(OM1.p2, OM2.p1, L=OUTPUT.length_OM1_OM2)
        model.connect(OM2.p2, OM3.p1, L=OUTPUT.length_OM2_OM3)
        model.connect(OM3.p2, OMC_input_port, L=OUTPUT.length_OM3_OMC)

        self.add_AS_telescopes(model)

        self.AS_port = OM3.p3
        self.OFI_sqz_port = OFI.p2

    def add_OMC(self, model, params):
        OMC_IC = model.add(
            fc.Beamsplitter(
                "OMC_IC",
                T=params.IC.T,
                L=params.IC.L,
                Rc=params.IC.Rc,
                alpha=params.IC.AOI,
            )
        )
        OMC_OC = model.add(
            fc.Beamsplitter(
                "OMC_OC",
                T=params.OC.T,
                L=params.OC.L,
                Rc=params.OC.Rc,
                alpha=params.OC.AOI,
            )
        )
        OMC_CM1 = model.add(
            fc.Beamsplitter(
                "OMC_CM1",
                T=params.CM1.T,
                L=params.CM1.L,
                Rc=params.CM1.Rc,
                alpha=params.CM1.AOI,
            )
        )
        OMC_CM2 = model.add(
            fc.Beamsplitter(
                "OMC_CM2",
                T=params.CM2.T,
                L=params.CM2.L,
                Rc=params.CM2.Rc,
                alpha=params.CM2.AOI,
            )
        )

        model.connect(OMC_IC.p3, OMC_OC.p1, L=params.length_IC_OC)
        model.connect(OMC_OC.p2, OMC_CM1.p1, L=params.length_OC_CM1)
        model.connect(OMC_CM1.p2, OMC_CM2.p1, L=params.length_CM1_CM2)
        model.connect(OMC_CM2.p2, OMC_IC.p4, L=params.length_CM2_IC)

        model.add(fc.Cavity("cavOMC", OMC_IC.p3.o, via=OMC_OC.p1.i, priority=100))

        self.OMC_input_port = OMC_IC.p1
        self.OMC_output_port = OMC_OC.p3

    def add_filter_cavity(self, model, params):
        FC1 = model.add(fc.Mirror("FC1", T=params.FC1.T, L=0, Rc=params.FC1.Rc))
        FC1AR = model.add(
            fc.Mirror(
                "FC1AR",
                T=1,
                L=0,
                Rc=params.FC1.Rc_AR,
                xbeta=FC1.xbeta.ref,
                ybeta=FC1.ybeta.ref,
                phi=FC1.phi.ref,
            )
        )
        model.connect(
            FC1AR.p1,
            FC1.p2,
            L=params.FC1.thickness,
            name="subFC1",
            nr=self.options.materials.test_mass_substrate.nr,
        )
        self._link_all_mechanical_dofs(FC1, FC1AR)

        FC_loss = model.add_parameter("FC_loss", params.FC_loss).ref
        model.add(fc.Mirror("FC2", T=params.FC2.T, L=FC_loss, Rc=params.FC2.Rc))
        model.connect(model.FC1.p1, model.FC2.p1, L=params.length_FC)
        model.add(fc.Cavity("cavFC", model.FC2.p1.o))
        fdetune = model.add_parameter("FC_detune_Hz", params.fdetune).ref
        model.FC2.phi = fdetune / model.cavFC.FSR * 180

    def add_squeeze_path(self, model, params, OFI_sqz_port):
        self.add_filter_cavity(model, params.FC)
        for bs in [f"ZM{idx}" for idx in range(1, 7)]:
            model.add(fc.Beamsplitter(bs, **params[bs]))
        model.add(fc.DirectionalBeamsplitter("SFI1"))
        model.add(fc.DirectionalBeamsplitter("SFI2"))

        if self.options.SQZ.add_VIP:
            raise NotImplementedError("This is kinda a mess")
        else:
            if self.options.SQZ.add_seed_laser:
                model.add(fc.Laser("SQZ_seed", P=1))
                model.connect(model.SQZ_seed.p1, model.SFI2.p1)
            else:
                model.connect(model.SFI1.p4, model.SFI2.p1)
            self.VIP_OUT_A = model.SFI1.p3
            self.VIP_OUT_B = model.SFI2.p3
            self.SQZ_inj_port = model.SFI1.p1

        model.connect(
            self.VIP_OUT_A, model.ZM1.p1, L=params.length_VIP_ZM1, name="VIP_ZM1"
        )
        model.connect(
            model.ZM1.p2, model.ZM2.p1, L=params.length_ZM1_ZM2, name="ZM1_ZM2"
        )
        model.connect(
            model.ZM2.p2, model.ZM3.p1, L=params.length_ZM2_ZM3, name="ZM2_ZM3"
        )
        model.connect(
            model.ZM3.p2, model.FC1AR.p2, L=params.length_ZM3_FC1, name="ZM3_FC1"
        )
        model.connect(
            self.VIP_OUT_B, model.ZM4.p1, L=params.length_VIP_ZM4, name="VIP_ZM4"
        )
        model.connect(
            model.ZM4.p2, model.ZM5.p1, L=params.length_ZM4_ZM5, name="ZM4_ZM5"
        )
        model.connect(
            model.ZM5.p2, model.ZM6.p1, L=params.length_ZM5_ZM6, name="ZM5_ZM6"
        )
        model.connect(model.ZM6.p2, OFI_sqz_port, params.length_ZM6_OFI, name="ZM6_OFI")

        model.add(fc.Squeezer("SQZ", db=params.db, angle=params.angle))
        model.connect(model.SQZ.p1, self.SQZ_inj_port, 0)

        if self.options.SQZ.add_detectors:
            model.add(QuantumNoiseDetector("AS_p1_qnoise", model.AS.p1.i, nsr=False))

    def add_LSC(self, model):
        if self.options.LSC.add_DOFs:
            self.add_LSC_DOFs(model)
        if self.options.LSC.add_readouts:
            self.add_LSC_readouts(model)
        if self.options.LSC.add_locks:
            self.add_LSC_DC_locks(model)
        if self.options.LSC.add_AC_loops:
            self.add_LSC_AC_loops(model)

    def add_ASC(self, model):
        if self.options.ASC.add_DOFs:
            self.add_ASC_DOFs(model)
        if self.options.ASC.add_readouts:
            self.add_ASC_readouts(model)
        if self.options.ASC.add_locks:
            self.add_ASC_DC_locks(model)
        if self.options.ASC.add_AC_loops:
            self.add_ASC_AC_loops(model)

    def add_LSC_DOFs(self, model):
        drives = self.local_drives["L"].copy()
        ldofs = {}
        for optic_name in drives.keys():
            optic = model.get(optic_name)
            # Setup local DOF for reading out optic motion but driving
            # whatever point has been requested
            ldofs[optic_name] = LocalDegreeOfFreedom(
                f"{optic_name}.ldof.z",
                DC=optic.phi,
                AC_OUT=optic.mech.z,
                # No AC IN connections, these are made later with the
                # input/output matrices and blending
                AC_IN=None,
            )

        for dof_name in self.LSC_output_matrix:
            # Add in DC drives for the DOFs
            dof_drives = [
                ldofs[k] for k, v in self.LSC_output_matrix[dof_name].items() if v != 0
            ]
            amplitudes = [
                v for v in self.LSC_output_matrix[dof_name].values() if v != 0
            ]
            assert len(dof_drives) == len(amplitudes)
            model.add(fc.DegreeOfFreedom(dof_name, *roundrobin(dof_drives, amplitudes)))

    def add_ASC_DOFs(self, model):
        for Y_P, dc_attr_name, ac_attr_name in zip(
            ["Y", "P"], ["xbeta", "ybeta"], ["yaw", "pitch"]
        ):
            _rx, _ry = finesse_ligo.asc.arm_r_factors(model, Y_P)

            rx = model.add_parameter(f"rx_{Y_P}", _rx).ref
            ry = model.add_parameter(f"ry_{Y_P}", _ry).ref

            drives = self.local_drives[Y_P].copy()
            ldofs = {}

            for optic_name in drives.keys():
                # Setup local DOF for reading out optic motion but driving
                # whatever point has been requested
                ldofs[optic_name] = LocalDegreeOfFreedom(
                    f"{optic_name}.ldof.{Y_P.lower()}",
                    DC=model.get(f"{optic_name}.{dc_attr_name}"),
                    AC_OUT=model.get(f"{optic_name}.mech.{ac_attr_name}"),
                    # No AC IN connections, these are made later with the
                    # input/output matrices and blending
                    AC_IN=None,
                )

            for dof_name in self.ASC_output_matrix:
                if dof_name.endswith(Y_P):
                    # Add in DC drives for the DOFs
                    fn = lambda x: eval(x, {"rx_" + Y_P: rx, "ry_" + Y_P: ry})
                    dof_drives = [
                        ldofs[k]
                        for k, v in self.ASC_output_matrix[dof_name].items()
                        if v != 0
                    ]
                    amplitudes = [
                        fn(str(v))
                        for v in self.ASC_output_matrix[dof_name].values()
                        if v != 0
                    ]
                    assert len(dof_drives) == len(amplitudes)
                    model.add(
                        fc.DegreeOfFreedom(
                            dof_name, *roundrobin(dof_drives, amplitudes)
                        )
                    )

    def add_LSC_readouts(self, model):
        pass

    def add_ASC_readouts(self, model):
        pass

    def add_LSC_DC_locks(self, model):
        pass

    def add_ASC_DC_locks(self, model):
        pass

    def add_LSC_AC_loops(self, model):
        pass

    def add_ASC_AC_loops(self, model):
        pass

    def _add_AC_loops(
        self, model, drives, input_matrix, output_matrix, controller, options
    ):
        from finesse.components.electronics import ZPKFilter

        vars = {p.name: p.ref for p in model.parameters}

        def make_filter_name(name_to, name_fr, description):
            name = "___{:s}__{:s}__{:s}".format(
                name_to.replace(".", "_"),
                name_fr.replace(".", "_"),
                description,
            )
            return name

        # First we go and add any ZPK filters requested for driving the
        # suspension points
        for optic_name, sus_drives in drives.items():
            assert optic_name in model.elements
            for sus_node_name, v in sus_drives.items():
                sus_node = model.get(sus_node_name)
                if hasattr(v, "__len__"):
                    if len(v) != 4:
                        raise ValueError(
                            "Drives must either be a scalar or a "
                            "four tuple (filter_name, z, p, k), but "
                            f"{sus_node_name} has length {len(v)}"
                        )
                    name, z, p, k = v
                else:
                    name = make_filter_name(sus_node_name, optic_name, "drive_filter")
                    z = []
                    p = []
                    k = v
                ZPK = model.add(ZPKFilter(name, z, p, k))
                model.connect(ZPK.p2.o, sus_node)

        # now connect the DOF drives to the local drives
        for dof_name in output_matrix:
            DOF = model.get(dof_name)
            for optic_name, amplitude in output_matrix[dof_name].items():
                amplitude = eval(str(amplitude), vars)
                sus_drives = drives[optic_name]
                for sus_node_name, v in sus_drives.items():
                    sus_node = model.get(sus_node_name)
                    if hasattr(v, "__len__"):
                        if len(v) != 4:
                            raise ValueError(
                                "Output matrix must either be a scalar or a "
                                "four tuple (filter_name, z, p, k), but "
                                f"{optic_name} has length {len(v)}"
                            )
                        name = v[0]
                    else:
                        name = make_filter_name(
                            sus_node_name, optic_name, "drive_filter"
                        )
                    ZPK = model.get(name)
                    model.connect(DOF.AC.i, ZPK.p1, gain=amplitude)

        added_controllers = {}
        for dof_name in controller:
            DOF = model.get(dof_name)
            name, z, p, k = controller[dof_name]
            ZPK = model.add(ZPKFilter(name, z, p, k))
            model.connect(ZPK.p2.o, DOF.AC.i)
            added_controllers[dof_name] = ZPK.p1.i

        # now connect the DOF drives to the local drives
        for dof_name in input_matrix:
            DOF = model.get(dof_name)
            connect_to = added_controllers.get(dof_name, DOF.AC.i)

            for readout_comp_name, v in input_matrix[dof_name].items():
                node = model.get(readout_comp_name)
                # make the connection
                if hasattr(v, "__len__"):
                    if len(v) != 4:
                        raise ValueError(
                            "Input matrix must either be a scalar or a "
                            "four tuple (filter_name, z, p, k), but "
                            f"{readout_comp_name} has length {len(v)}"
                        )
                    name, z, p, k = v
                else:
                    z = []
                    p = []
                    k = v
                    name = make_filter_name(readout_comp_name, dof_name, "input_filter")
                ZPK = model.add(ZPKFilter(name, z, p, k))
                model.connect(node, ZPK.p1)
                if options.close_AC_loops:
                    model.connect(ZPK.p2.o, connect_to)

    def set_surface_map(
        self,
        optic,
        gen_surface_map,
        optic_id=None,
        nan_value=0,
        make_axisymmetric=False,
    ):
        """Add a surface map to an optic and optionally adds the surface profile of an
        optic measured by a Zygo interferometer using
        get_test_mass_surface_profile_interpolated.

        Parameters
        ----------
        optic : :class:`Mirror`, :class:`Beamsplitter`, :class:`Lens`
            Optic to add the surface maps to
        gen_surface_map : callable
            Function to generate amplitude maps. It should return a 1D array of dimensions
            of a map in both directions and a 2D amplitude map
        test_mass : str, optional
            If not None, LIGO test mass name and serial number specifying the surface profile
            to add
        nan_value : float, optional
            Value to replace NaNs with to allow for interpolation beyond radius
        make_axisymmetric : bool, optional
            If True, return a 2D interpolator that averages the surface profile radially.
            This makes the map axisymmetric to remove any odd-mode couplings.
        """
        x, amplitude = gen_surface_map()
        y = x
        if optic_id is not None and self.options.apertures.use_surface_profile:
            opd = maps.get_test_mass_surface_profile_interpolated(
                test_mass=optic_id,
                nan_value=nan_value,
                make_axisymmetric=make_axisymmetric,
            )(x, y)
        else:
            opd = None
        surface_map = Map(x, y, amplitude=amplitude, opd=opd)
        if hasattr(optic, "surface_map"):
            optic.surface_map = surface_map
        elif hasattr(optic, "OPD_map"):
            optic.OPD_map = surface_map
        else:
            raise ValueError(f"Cannot set a map for {optic.name}")
        if hasattr(optic, "misaligned") and isinstance(optic, Mirror):
            # For test masses to always recompute
            # bit of a hack at the moment in FINESSE
            optic.misaligned.is_tunable = True


class CoupledCavityFactory(DRFPMIFactory):
    pass
