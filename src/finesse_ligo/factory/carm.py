from munch import Munch
import finesse
import finesse.components as fc
from finesse.detectors import MathDetector, PowerDetector, AmplitudeDetector
from finesse.symbols import Constant
import finesse.analysis.actions as fa

from .base import DRFPMIFactory


class CARMFactory(DRFPMIFactory):
    def reset(self):
        self.set_default_options()
        self.set_default_drives()
        self.set_default_LSC()

    def set_default_options(self):
        """Resets factory to all default options."""

        super().set_default_options()

        self.options.LSC.add_DOFs = True
        self.options.LSC.add_readouts = True
        self.options.LSC.add_output_detectors = False
        self.options.LSC.add_locks = True
        self.options.LSC.add_AC_loops = False
        self.options.LSC.close_AC_loops = False

    def set_default_drives(self):
        """Resets factory to all default drives."""
        self.local_drives = Munch(
            {
                "L": Munch(
                    {
                        "ITMY": {"ITMY.mech.z": 1},
                        "ETMY": {"ETMY.mech.z": 1},
                        "PRM": {"PRM.mech.z": 1},
                        "BS": {"BS.mech.z": 1},
                    }
                ),
            }
        )

    def set_default_LSC(self):
        """Resets factory to all default LSC settings."""
        self.LSC_output_matrix = Munch(
            {
                "YARM": Munch(),
                "CARM": Munch(),
                "PRCL": Munch(),
            }
        )

        self.LSC_output_matrix.YARM.ETMY = +1
        self.LSC_output_matrix.CARM.ETMY = +1
        self.LSC_output_matrix.PRCL.PRM = +1

        self.LSC_input_matrix = Munch(
            {
                "YARM": Munch(),
                "CARM": Munch(),
                "PRCL": Munch(),
            }
        )

        self.LSC_input_matrix.CARM.REFL9_I = 1
        self.LSC_input_matrix.PRCL.POP9_I = 1

        self.LSC_controller = Munch()

    def make(self):
        base = finesse.Model()
        self.pre_make(base, self.params)
        self.add_arm_cavity(base, "Y", self.params.Y, self.options)
        self.add_BS(base, self.params.BS)
        self.add_MICH(base, "Y", self.params.Y, self.BS_Y, self.ITMY_AR_fr)
        self.add_PRC(base, self.params.PRC, self.BS_PR, self.options)

        self.add_input_path(base, self.params.INPUT, self.PRM_AR_fr)
        self.add_cavities(base)

        self.add_suspensions(base)
        self.add_LSC(base)

        if self.options.add_detectors:
            self.add_detectors(base)

        self.post_make(base, self.params)
        base.beam_trace()
        return base

    def add_BS(self, model, params):
        super().add_BS(model, params)
        model.BS.T = 0
        model.BS.L = 0

    def add_cavities(self, model):
        model.add(fc.Cavity("cavYARM", model.ETMY.p1.o, priority=100))
        model.add(fc.Cavity("cavPRY", self.PRM_HR_fr.o, via=self.ITMY_HR_bk.i))

    def add_suspensions(self, model):
        if self.options.QUAD_suspension_model is not None:
            raise NotImplementedError()

    def add_LSC_readouts(self, model):
        f1 = model.f1.ref
        f2 = model.f2.ref
        output_detectors = self.options.LSC.add_output_detectors
        model.add(
            fc.ReadoutRF(
                "REFL9", self.REFL_port.o, f=f1, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "REFL45", self.REFL_port.o, f=f2, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "POP9", self.POP_port.o, f=f1, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "POP18", self.POP_port.o, f=2 * f1, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "POP36", self.POP_port.o, f=f2 - f1, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "POP45", self.POP_port.o, f=f2, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "POP90", self.POP_port.o, f=2 * f2, output_detectors=output_detectors
            )
        )

    def add_LSC_DC_locks(self, model):
        model.add(
            finesse.locks.Lock(
                "CARM_lock", model.REFL9.outputs.I, model.CARM.DC, -0.1, 1e-6
            )
        )
        model.add(
            finesse.locks.Lock(
                "PRCL_lock", model.POP9.outputs.I, model.PRCL.DC, 2.8, 1e-6
            )
        )

    def add_LSC_AC_loops(self, model):
        raise NotImplementedError()

    def add_detectors(self, model):
        f1 = model.f1.ref
        f2 = model.f2.ref

        def add_amplitude_detectors(port, key):
            freqs = []
            if self.options.add_9MHz:
                freqs.append((f1, "9"))
            if self.options.add_45MHz:
                freqs.append((f2, "45"))
            self._add_all_amplitude_detectors(model, port, key, *freqs)

        model.add(PowerDetector("Pin", self.PRM_AR_fr.i))
        Py = Constant(model.Py)

        model.add(PowerDetector("Prefl", self.REFL_port.o))
        model.add(PowerDetector("Ppop", self.POP_port.o))
        Piny = Constant(model.Piny)

        add_amplitude_detectors(self.POP_port.o, "pop")
        add_amplitude_detectors(model.PRM.p2.i, "in")
        add_amplitude_detectors(self.REFL_port.o, "refl")
        add_amplitude_detectors(model.PRM.p1.o, "prc")

        model.add(AmplitudeDetector("a_carrier_refl", self.REFL_port.o, f=0))
        a_carrier_piny = Constant(model.a_carrier_piny)
        a_carrier_y = Constant(model.a_carrier_y)

        # FIXME: there should be some logic below for if 9 and 45 MHz have been added
        Pprc_carrier = abs(Constant(model.a_c0_prc)) ** 2
        Pin_carrier = abs(Constant(model.a_c0_in)) ** 2
        Pprc_9 = abs(Constant(model.a_u9_prc)) ** 2 + abs(Constant(model.a_l9_prc)) ** 2
        Pprc_45 = (
            abs(Constant(model.a_u45_prc)) ** 2 + abs(Constant(model.a_l45_prc)) ** 2
        )
        Pin_9 = abs(Constant(model.a_u9_in)) ** 2 + abs(Constant(model.a_l9_in)) ** 2
        Pin_45 = abs(Constant(model.a_u45_in)) ** 2 + abs(Constant(model.a_l45_in)) ** 2
        Piny_carrier = abs(a_carrier_piny) ** 2

        model.add(MathDetector("Pprc_carrier", Pprc_carrier))
        model.add(MathDetector("Pprc_9", Pprc_9))
        model.add(MathDetector("Pprc_45", Pprc_45))
        model.add(MathDetector("Pin_carrier", Pin_carrier))
        model.add(MathDetector("Pin_9", Pin_9))
        model.add(MathDetector("Pin_45", Pin_45))
        model.add(MathDetector("Piny_carrier", Piny_carrier))

        model.add(MathDetector("PRG", Pprc_carrier / Pin_carrier))
        model.add(MathDetector("PRG9", Pprc_9 / Pin_9))
        model.add(MathDetector("PRG45", Pprc_45 / Pin_45))
        model.add(MathDetector("AGY", abs(a_carrier_y) ** 2 / Piny_carrier))
        model.add(MathDetector("cost_prcl", Pprc_9 * Py))

    def pre_make(self, model, params):
        pass

    def post_make(self, model, params):
        with finesse.symbols.simplification(allow_flagged=True):
            ly = model.path(self.BS_HR_Y.o, self.ITMY_HR_bk.i, symbolic=True)
            l_y = model.add(
                fc.Variable("l_y", ly.optical_length, description="BS HR to ITMY HR")
            )
            ppy = model.path(self.PRM_HR_fr.o, self.ITMY_HR_bk.i, symbolic=True)
            model.add(
                fc.Variable(
                    "L_PRC",
                    ppy.optical_length.collect(),
                    description="Average optical length of the PRC",
                )
            )


def InitialLock(
    LSC_demod_opt=(
        "CARM",
        "REFL9_I",
        "PRCL",
        "POP9_I",
    ),
    run_locks=True,
    exception_on_lock_fail=True,
    gain_scale=0.5,
    lock_steps=2000,
    pseudo_lock_arms=True,
):
    """Initial locking action, tries to somewhat replicate the locking prodcedure for
    LSC.

    If it can't find a good operating point then the RunLocks step at the end will fail.
    """
    if pseudo_lock_arms:
        arm_locks = (
            fa.PseudoLockCavity(
                "cavYARM", lowest_loss=False, feedback="YARM.DC", name="lock YARM"
            ),
        )
    else:
        arm_locks = (fa.Maximize("a_carrier_00_y", "YARM.DC"),)

    action = fa.Series(
        fa.Change(
            {
                "PRM.misaligned": True,
            },
            name="misalign PRM",
        ),
        # Lock each arm cavity to the lowest loss mode
        *arm_locks,
        # Realign the PRM
        fa.Change({"PRM.misaligned": False}, name="align PRM"),
        # get the PRC in roughly the right place whilst keeping arms on resonance
        fa.Maximize("PRG", "PRCL.DC", name="maximise PRG"),
        # get the PRC in roughly the right place whilst keeping arms on resonance
        fa.Maximize("cost_prcl", ["PRCL.DC", "CARM.DC"], name="maxmize Parm*PRG9"),
        fa.Noxaxis(name="after PRC"),
        fa.OptimiseRFReadoutPhaseDC(*LSC_demod_opt),
        fa.SetLockGains(d_dof_gain=1e-10, gain_scale=gain_scale),
        fa.RunLocks(
            max_iterations=lock_steps, exception_on_fail=exception_on_lock_fail
        ),
    )
    if not run_locks:
        action.actions = action.actions[:-1]
    return action
