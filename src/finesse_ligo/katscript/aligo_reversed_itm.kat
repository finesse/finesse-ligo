# modulators for core interferometer sensing - Advanced LIGO, CQG, 2015
# http://iopscience.iop.org/article/10.1088/0264-9381/32/7/074001/meta#cqg507871s4-8
# 9MHz (CARM, PRC, SRC loops)
variable f1 9099471
variable f2 5*f1
variable f3 13*f1
variable nsilica 1.45
variable Xloss 60u
variable Yloss 60u

###############################################################################
###   length definitions
###############################################################################
variable Larm 3994.47
variable LPR23 16.164  # distance between PR2 and PR3
variable LSR23 15.443  # distance between SR2 and SR3
variable LPR3BS 19.538 # distance between PR3 and BS
variable LSR3BS 19.366 # distance between SR3 and BS
variable lmich 5.342   # average length of MICH
variable lschnupp 0.08 # double pass schnupp length
variable lPRC (3+0.5)*c0/(2*f1) # T1000298 Eq2.1, N=3
variable lSRC (17)*c0/(2*f2) # T1000298 Eq2.2, M=3

###############################################################################
###   laser
###############################################################################
laser L0 P=125
mod mod1 f=f1 midx=0.18 order=1 mod_type=pm
mod mod2 f=f2 midx=0.18 order=1 mod_type=pm
mod mod3 f=f3 midx=0.024 order=1 mod_type=pm

# We do not actually need IM1 as it is a flat mirror
bs IM2 R=1 L=0 Rc=12.8
dbs IFI
bs IM3 R=1 L=0 Rc=-6.24
bs IM4 R=1 L=0
# Lengths https://dcc.ligo.org/DocDB/0142/T1700227 [1]
link(
    L0, mod1, mod2, mod3,
    0.4282+1.2938, # Length from old finesse file, distance from IMC output AR surface to IM2
    IM2, 0.260, IFI.p1, IFI.p3, 0.91, IM3, 1.175, IM4 # these lengths from [1]
)
# This the the IMC eigenmode calculated from a Finesse model of the IMC
# the beam is that which comes out of the MC3 AR surface towards IM1
gauss IMC L0.p1.o zx=-0.19382433126536727 zy=-0.17422413793103259 zrx=13.311179040699912 zry=13.319547688911488 priority=100

###############################################################################
###   PRC
###############################################################################
s sPRCin IM4.p2 PRMAR.p1 L=0.413
m PRMAR R=0 L=40u xbeta=PRM.xbeta ybeta=PRM.ybeta phi=PRM.phi
s sPRMsub1 PRMAR.p2 PRM.p1 L=0.0737 nr=nsilica
m PRM T=0.03 L=8.5u Rc=11.009
s lp1 PRM.p2 PR2.p1 L=lPRC-LPR3BS-LPR23-lmich
bs PR2 T=250u L=0 alpha=-0.79 Rc=-4.545
s lp2 PR2.p2 PR3.p1 L=LPR23
bs PR3 T=0 L=0 alpha=0.615 Rc=36.027
s lp3 PR3.p2 BS.p1 L=LPR3BS

###############################################################################
###   BS
###############################################################################
bs BS R=0.5 L=0 alpha=45
s BSsub1 BS.p3 BSAR1.p1 L=60m/cos(radians(29.186885954108114)) nr=nsilica
s BSsub2 BS.p4 BSAR2.p2 L=60m/cos(radians(29.186885954108114)) nr=nsilica
bs BSAR1 L=50u R=0 alpha=29.186885954108114
bs BSAR2 L=50u R=0 alpha=BSAR1.alpha

###############################################################################
###   Yarm
###############################################################################
# Distance from beam splitter to Y arm input mirror
s ly1 BS.p2 ITMYlens.p1 L=lmich-lschnupp/2-ITMYsub.L*ITMXsub.nr
lens ITMYlens f=34500
s ly2 ITMYlens.p2 ITMYAR.p2
m ITMYAR R=0 L=20u xbeta=ITMY.xbeta ybeta=ITMY.ybeta phi=ITMY.phi
s ITMYsub ITMYAR.p1 ITMY.p2 L=0.2 nr=nsilica
m ITMY T=0.014 L=Yloss/2 R=1-ITMY.T-ITMY.L Rc=1934
s LY ITMY.p1 ETMY.p1 L=Larm
m ETMY T=5u L=Yloss/2 R=1-ETMY.T-ETMY.L Rc=2245
cav cavYARM ETMY.p1.o

###############################################################################
###   Xarm
###############################################################################
# Distance from beam splitter to X arm input mirror
s lx1 BSAR1.p3 ITMXlens.p1 L=lmich+lschnupp/2-ITMXsub.L*ITMXsub.nr-BSsub1.L*BSsub1.nr
lens ITMXlens f=34500
s lx2 ITMXlens.p2 ITMXAR.p2
m ITMXAR R=0 L=20u xbeta=ITMX.xbeta ybeta=ITMX.ybeta phi=ITMX.phi
s ITMXsub ITMXAR.p1 ITMX.p2 L=0.2 nr=nsilica
m ITMX T=0.014 L=Xloss/2 R=1-ITMX.T-ITMX.L Rc=1934
s LX ITMX.p1 ETMX.p1 L=Larm
m ETMX T=5u L=Xloss/2 R=1-ETMX.T-ETMX.L Rc=2245
cav cavXARM ETMX.p1.o

###############################################################################
###   SRC
###############################################################################
s ls3 BSAR2.p4 SR3.p1 L=LSR3BS
bs SR3 T=0 L=0 alpha=0.785 Rc=35.972841
s ls2 SR3.p2 SR2.p1 L=LSR23
bs SR2 T=0 L=0 alpha=-0.87 Rc=-6.406
s ls1 SR2.p2 SRM.p1 L=lSRC-LSR3BS-LSR23-BSsub2.L*BSsub2.nr-lmich
m SRM T=0.32 L=8.7u Rc=-5.6938
s SRMsub SRM.p2 SRMAR.p1 L=0.0749 nr=nsilica
m SRMAR R=0 L=50n

###############################################################################
###   Output path
###############################################################################
dbs OFI
sq sqz db=6 angle=90.0
# https://dcc.ligo.org/DocDB/0163/T1900649/008/FCLayout_T1900649-v8.pdf
# Filter cavity optics
lens ZM5 f=3.4/2 # f=Rc/2
lens ZM4 f=13.33/2
link(
    sqz,
    ZM4,
    66.69*25.4m,
    ZM5,
    186.3*25.4m + 2.1, #distance from ZM5->ZM6 + ZM6->SRM
    OFI.p2
)

# (as built parameters: D1300507-v1)
s sSRM_OFI SRMAR.p2 OFI.p1 L=0.7278
s sOFI_OM1 OFI.p3 OM1.p1 L=2.9339

bs OM1 T=800u L=0 alpha=2.251 Rc=[4.6, 4.6]
s sOM1_OM2 OM1.p2 OM2.p1 L=1.395
bs OM2 T=0 L=0 alpha=4.399 Rc=[1.7058, 1.7058]
s sOM2_OM3 OM2.p2 OM3.p1 L=0.631
bs OM3 T=0.01 L=0 alpha=30.037
s sOM3_OMC OM3.p2 OMC_IC.p1 L=0.2034

###############################################################################
###   OMC
###############################################################################
# obp OMC fc=0 bandwidth=1M filter_hom=[0,0]
# link(SRMAR.p2, OFI.p1)
# link(OFI.p3, OMC)

cav cavOMC OMC_IC.p3.o via=OMC_OC.p1.i
bs OMC_IC T=0.0076 L=10u alpha=2.7609
s lIC_OC OMC_IC.p3 OMC_OC.p1 L=0.2815
bs OMC_OC T=0.0075 L=10u alpha=4.004
s lOC_CM1 OMC_OC.p2 OMC_CM1.p1 L=0.2842
bs OMC_CM1 T=36u L=10u alpha=4.004 Rc=[2.57321, 2.57321]
s lCM1_CM2 OMC_CM1.p2 OMC_CM2.p1 L=0.2815
bs OMC_CM2 T=35.9u L=10u alpha=4.004 Rc=[2.57369, 2.57369]
s lCM2_IC OMC_CM2.p2 OMC_IC.p4 L=0.2842


###############################################################################
### Length sensing and control
###############################################################################
dof XARM ETMX.dofs.z
dof YARM ETMY.dofs.z
dof CARM ETMX.dofs.z +1 ETMY.dofs.z +1
dof DARM ETMX.dofs.z +1 ETMY.dofs.z -1
dof PRCL PRM.dofs.z +1
dof SRCL SRM.dofs.z +1 DC=90
dof MICH BS.dofs.z +1
dof MICH2 ITMY.dofs.z -1 ETMY.dofs.z +1 ITMX.dofs.z +1 ETMX.dofs.z -1
dof STRAIN LX.dofs.h +1 LY.dofs.h -1
dof FRQ L0.dofs.frq
dof RIN L0.dofs.amp

readout_rf REFL9 PRMAR.p1.o f=f1
readout_rf REFL45 PRMAR.p1.o f=5*f1
readout_rf POP9  PR2.p3.o   f=f1
readout_rf POP18  PR2.p3.o  f=2*f1
readout_rf POP36 PR2.p3.o   f=f2-f1
readout_rf POP45 PR2.p3.o   f=f2
readout_rf POP90 PR2.p3.o   f=2*f2
readout_rf AS45  SRMAR.p2.o f=f2
readout_rf AS36  SRMAR.p2.o f=f2-f1
readout_dc AS    OMC_OC.p3.o

lock CARM_lock REFL9.outputs.I CARM.DC -0.1 1e-6
lock MICH_lock REFL45.outputs.I MICH2.DC -15 1e-6
lock PRCL_lock POP9.outputs.I PRCL.DC 2.8 1e-6
lock SRCL_lock POP45.outputs.I SRCL.DC 42 1e-6
lock DARM_rf_lock AS45.outputs.I DARM.DC -0.003 1e-6
lock DARM_dc_lock AS.outputs.DC DARM.DC -0.003 1e-6 offset=20m enabled=false

###############################################################################
### DC power measurements
###############################################################################
pd Pin PRM.p1.i
pd Px ITMX.p1.o
pd Py ITMY.p1.o
pd Pprc PRM.p2.o
pd Psrc SRM.p1.i
pd Prefl PRM.p1.o
pd Ppop PR2.p3.o
pd Pas_c OM1.p1.i
pd Pas OMC_OC.p3.o
pd Pinx ITMX.p1.i
pd Piny ITMY.p1.i

ad a_u9_pop PR2.p3.o f=f1
ad a_l9_pop PR2.p3.o f=-f1
ad a_u45_pop PR2.p3.o f=f2
ad a_l45_pop PR2.p3.o f=-f2

ad a_u9_as SRM.p2.o f=f1
ad a_l9_as SRM.p2.o f=-f1
ad a_u45_as SRM.p2.o f=+f2
ad a_l45_as SRM.p2.o f=-f2

ad a_u9_in PRM.p1.i f=+f1
ad a_l9_in PRM.p1.i f=-f1
ad a_u45_in PRM.p1.o f=f2
ad a_l45_in PRM.p1.o f=-f2

ad a_u9_refl PRM.p1.o f=f1
ad a_l9_refl PRM.p1.o f=-f1
ad a_u45_refl PRM.p1.o f=f2
ad a_l45_refl PRM.p1.o f=-f2

ad a_carrier_in PRM.p1.i f=0
ad a_carrier_refl PRM.p1.o f=0
ad a_carrier_prc PRM.p2.o f=0
ad a_carrier_src SRM.p1.i f=0
ad a_carrier_pinx ITMX.p2.i f=0
ad a_carrier_piny ITMY.p2.i f=0
ad a_carrier_y ITMY.p1.i f=0
ad a_carrier_x ITMX.p1.i f=0
ad a_carrier_00_x ITMX.p1.i f=0 n=0 m=0
ad a_carrier_00_y ITMY.p1.i f=0 n=0 m=0

ad a_u9_prc PRM.p2.o f=+f1
ad a_l9_prc PRM.p2.o f=-f1
ad a_u45_prc PRM.p2.o f=+f2
ad a_l45_prc PRM.p2.o f=-f2

mathd Pprc_carrier abs(a_carrier_prc)**2
mathd Pprc_9 abs(a_u9_prc)**2+abs(a_l9_prc)**2
mathd Pprc_45 abs(a_u45_prc)**2+abs(a_l45_prc)**2
mathd Pin_carrier abs(a_carrier_in)**2
mathd Pin_9 abs(a_u9_in)**2+abs(a_l9_in)**2
mathd Pin_45 abs(a_u45_in)**2+abs(a_l45_in)**2
mathd Pinx_carrier abs(a_carrier_pinx)**2
mathd Piny_carrier abs(a_carrier_piny)**2
mathd Pas_carrier abs(a_carrier_src)**2

mathd PRG Pprc_carrier/Pin_carrier
mathd PRG9 Pprc_9/Pin_9
mathd PRG45 Pprc_45/Pin_45
mathd AGX abs(a_carrier_x)**2/Pinx_carrier
mathd AGY abs(a_carrier_y)**2/Piny_carrier

ad a_bs_x_in BS.p3.i f=0
ad a_bs_y_in BS.p2.i f=0

cav cavPRX PRM.p2.o via=ITMX.p2.i
cav cavPRY PRM.p2.o via=ITMY.p2.i
cav cavSRX SRM.p1.o via=ITMX.p2.i
cav cavSRY SRM.p1.o via=ITMY.p2.i
