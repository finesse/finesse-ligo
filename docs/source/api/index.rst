.. include:: /defs.hrst

API Documentation
=================

This reference manual details the modules, classes and functions included in
|Finesse| LIGO, describing what they are, what they do and (where relevant)
mathematical descriptions of how they work.

.. autosummary::
    :toctree: .

    finesse_ligo
