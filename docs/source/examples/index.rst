.. include:: /defs.hrst
.. _examples:

Examples
--------

.. toctree::
    :maxdepth: 1
    :name: sec-intro

    plot_darm
    plot_hard
    SRCL_dither
    schnupp_comparison
