.. include:: /defs.hrst
.. _examples_plot_darm:

Plot DARM transfer function
---------------------------

.. jupyter-execute::

    import numpy as np
    import finesse
    import finesse_ligo
    import finesse.analysis.actions as fac
    import matplotlib.pyplot as plt

    from finesse_ligo.factory import aligo
    from finesse.plotting import bode
    from finesse_ligo.suspension import QUADSuspension

    finesse.init_plotting()

    # We first make a factory object that can generate an ALIGO model
    # here we do so using the LHO O4 parameter file
    factory = aligo.ALIGOFactory("lho_O4.yaml")

    # The factory now produces a model that can be used
    factory.reset()
    lho = factory.make()
    lho.modes("off")  # planewave
    lho.run(fac.Series(aligo.InitialLock(), aligo.DARM_RF_to_DC()))

    # Now make a new LIGO model with suspensions
    factory.reset()
    factory.options.QUAD_suspension_model = finesse_ligo.suspension.QUADSuspension

    lho2 = factory.make()
    lho2.modes("off")
    lho2.run(fac.Series(aligo.InitialLock(), aligo.DARM_RF_to_DC()))

    # Make a HOM model
    factory.reset()
    factory.options.QUAD_suspension_model = finesse_ligo.suspension.QUADSuspension
    lho3 = factory.make()
    lho3.modes("even", maxtem=2)
    lho3.run(fac.Series(aligo.InitialLock(), aligo.DARM_RF_to_DC()))

    analysis = fac.FrequencyResponse(
        np.geomspace(0.1, 10e3, 300), "DARM.AC.i", "AS.DC", open_loop=True
    )

    sol1 = lho.run(analysis)
    sol2 = lho2.run(analysis)
    sol3 = lho3.run(analysis)

    # %%
    axs = bode(sol1.f, sol1.out[:, 0, 0], db=False, label="No QUADs+planewave")
    bode(sol2.f, sol2.out[:, 0, 0], db=False, axs=axs, ls="--", label="QUADs+planewave")
    bode(sol3.f, sol3.out[:, 0, 0], db=False, axs=axs, ls="-.", label="QUADs+HOMs")

    plt.title("DARM sensing function")
    plt.ylabel("Magnitude [W/m]")
