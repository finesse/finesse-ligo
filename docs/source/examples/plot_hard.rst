.. include:: /defs.hrst
.. _examples_plot_hard:

Plot HARD angular transfer functions
------------------------------------

.. jupyter-execute::

    import finesse
    import finesse_ligo

    import numpy as np
    import finesse.analysis.actions as fac
    import matplotlib.pyplot as plt

    from finesse.plotting import bode
    from finesse_ligo.factory import aligo
    from finesse_ligo.suspension import QUADSuspension

    finesse.init_plotting()

    factory = aligo.ALIGOFactory("lho_O4.yaml")
    factory.options.QUAD_suspension_model = QUADSuspension
    factory.options.ASC.add = True
    model1 = factory.make()
    factory.set_default_drives(P_torque=False, Y_torque=False)
    model2 = factory.make()

    for _model in [model1, model2]:
        _model.modes(maxtem=2)
        _model.fsig.f = 1

    F_Hz = np.geomspace(0.1, 50, 300)

    dofs = [
        'CHARD_P',
        'CSOFT_P',
        'DHARD_P',
        'DSOFT_P',
        'CHARD_Y',
        'CSOFT_Y',
        'DHARD_Y',
        'DSOFT_Y',
    ]

    inputs = [dof + ".AC.i" for dof in dofs]
    outputs = [dof + ".AC.o" for dof in dofs]

    actions = fac.Series(
        aligo.InitialLock(),
        aligo.DARM_RF_to_DC(),
        fac.FrequencyResponse(
            F_Hz,
            inputs,
            outputs,
            name="fresp",
        ),
    )

    sol1 = model1.run(actions)
    sol2 = model2.run(actions)

    tf1 = sol1["fresp"].out
    tf2 = sol2["fresp"].out

    for tf, kind in zip([tf1, tf2], ["torque", "angle"]):
        axs = bode(F_Hz, tf[:, 0, 0], db=False, label="CHARD", c="xkcd:cerulean")
        bode(F_Hz, tf[:, 1, 1], db=False, axs=axs, label="CSOFT", c="xkcd:blood red")
        bode(
            F_Hz,
            tf[:, 2, 2],
            db=False,
            axs=axs,
            label="DHARD",
            ls="--",
            c="xkcd:tangerine",
        )
        bode(
            F_Hz,
            tf[:, 3, 3],
            db=False,
            axs=axs,
            label="DSOFT",
            ls="--",
            c="xkcd:kelly green",
        )
        axs[0].set_title(f"Pitch {kind} input")

        axs = bode(F_Hz, tf[:, 4, 4], db=False, label="CHARD", c="xkcd:cerulean")
        bode(F_Hz, tf[:, 5, 5], db=False, axs=axs, label="CSOFT", c="xkcd:blood red")
        bode(
            F_Hz,
            tf[:, 6, 6],
            db=False,
            axs=axs,
            label="DHARD",
            ls="--",
            c="xkcd:tangerine",
        )
        bode(
            F_Hz,
            tf[:, 7, 7],
            db=False,
            axs=axs,
            label="DSOFT",
            ls="--",
            c="xkcd:kelly green",
        )
        axs[0].set_title(f"Yaw {kind} input")
