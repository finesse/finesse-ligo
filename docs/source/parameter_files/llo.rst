.. include:: /defs.hrst
.. _parameter_file_llo:

Livingston O4
=============

Select this parameter file using :code:`llo_O4.yaml` when making the factory.

.. literalinclude:: ../../../src/finesse_ligo/parameter_files/llo_O4.yaml
   :language: yaml
   :linenos:
