.. Finesse 3 documentation master file

.. only:: latex

    =======
    Finesse
    =======

.. only:: not latex

    .. include:: title.rst

    ----

.. toctree::
    :maxdepth: 1
    :name: sec-intro

    introduction/index

.. toctree::
    :maxdepth: 0
    :name: sec-installation

    installation/index

.. toctree::
    :maxdepth: 3
    :name: sec-getting-started

    getting_started/index

.. toctree::
    :titlesonly:
    :maxdepth: 2

    examples/index.rst

.. toctree::
    :titlesonly:
    :maxdepth: 2

    parameter_files/index.rst

.. toctree::
    :titlesonly:
    :maxdepth: 1

    appendix.rst

.. toctree::
    :maxdepth: 1
    :name: sec-back

    zglossary/index
