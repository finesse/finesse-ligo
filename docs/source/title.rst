.. include:: /defs.hrst

.. cssclass:: nobg
.. figure:: images/finesse_ligo_large.png
    :align: center

Overview
========

|Finesse| LIGO is a package that has developed to support modelling the LIGO
detectors. It is primarily concerned with modelling optical and control aspects
for complex interferometers.
