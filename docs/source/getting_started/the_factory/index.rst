.. include:: ../../defs.hrst
.. _the_factory:

The Factory
===========

This section will broadly highlight how the factory is meant to work and be
interacted with. Often using the factory will be spread throughout the examples,
but here a more generic view will be given.

The factory pattern was adopted to deal with the variety of scenarios that
people want to model LIGO in with |Finesse|. The factory is essentially a tool
used to build models. Models in |Finesse| are not easily reversible, as in often
it is not easy to disconnect, remove, and manipulate and already built model
This is because components generate many hidden connections behind the scenes
and altering it becomes challenging to handle in a sensible way.

Instead the best pattern is to just build a new model with the changes you need.
The factory can be specified with various options to build particular models.
It's important to note that the factory builds a particular layout for us and
fills it with parameters based on a YAML file, which is customised to different
detectors. But once a model has been made you can still interact with it like
you would any other |Finesse| model and changes parameters, add new components,
etc.

Next we'll see what those options are and briefly discuss what they do.

.. toctree::
    :maxdepth: 2

    options
    parameters
    local_drives
    ISC_input_output_matrices
    squeezer
