.. include:: /defs.hrst
.. _factory_parameters:

Factory parameters and the YAML file
====================================

The parameters that are being used by the factory can be seem by checking the
:code:`factory.params` attribute. This is a `Munch
<https://github.com/Infinidat/munch>`_ object which is a special dictionary that
can have its entries accessed like attributes to save typing so many :code:`[]`.

.. jupyter-execute::

    import pprint
    from finesse_ligo.factory import aligo
    factory = aligo.ALIGOFactory("llo_O4.yaml")

    pprint.PrettyPrinter(indent=0,width=20).pprint(
        factory.params.toDict()
    )

See :ref:`parameter_files` for the currently stored parameters and options. You
can also copy these parameters files and pass the filepath into
:code:`aligo.ALIGOFactory(path)` if you wish to edit the default values and
store them, or you can simply access and change them before making the model

.. jupyter-execute::

    factory.params.INPUT.add_IMC_and_IM1 = True

If you want to reset back to defaults you can call :code:`factory.reset()`.
