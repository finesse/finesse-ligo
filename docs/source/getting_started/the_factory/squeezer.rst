.. include:: /defs.hrst
.. _squeezer:

Squeeze Path and Filter Cavity
------------------------------

By default, the squeezer, filter cavity, and adaptive optics used to inject the squeezed
state into the interferometer are not included in the model. They can be added by
setting :code:`SQZ.add = True`. In this example we add these components and calculate
the quantum noise for different injected squeeze angles and different filter cavity
detunings.

Note that many applications only need the filter cavity and adaptive optics and do not
require the squeezer itself. Since calculating the quantum noise can be slow, the
:external:class:`~finesse.detectors.quantum_noise_detector.QuantumNoiseDetector` needed to do so is not added by default. It is
included in the model by setting :code:`SQZ.add_detectors = True`.

.. jupyter-execute::

   import numpy as np
   import matplotlib.pyplot as plt

   import finesse
   import finesse.analysis.actions as fa
   from finesse_ligo.factory import aligo
   from finesse_ligo.suspension import QUADSuspension
   from finesse.plotting import bode

   finesse.init_plotting()

   factory = aligo.ALIGOFactory("lho_O4.yaml")
   factory.options.SQZ.add = True
   factory.options.SQZ.add_detectors = True
   # add suspensions to see radiation pressure effects
   factory.options.QUAD_suspension_model = QUADSuspension
   model = factory.make()

After making the model, we calculate the plane-wave quantum noise by sweeping the signal
frequency :class:`fsig` with an :external:class:`~finesse.analysis.actions.axes.Xaxis` action. First the model needs to
be locked and transitioned to DC readout as described in :ref:`length_locking`. The
factory adds a parameter :code:`FC_detune_Hz` to the model which sets the detuning of
the filter cavity in Hz. We use a :external:class:`~finesse.analysis.actions.series.For` action to sweep through a range
of detunings. Finally, misaligning the filter cavity end mirror results in a
frequency independent squeezed state being injected into the interferometer.

Below we make use of the :external:meth:`~finesse.model.Model.temporary_parameters`
context manager to revert any parameters back to their default. This is useful when you
want to re-run the simulation. Putting the
:external:class:`~finesse.analysis.actions.series.For` action to sweep through the
detunings inside a
:external:class:`~finesse.analysis.actions.temporary.TemporaryParameters` action ensures
that the detuning is returned to the starting value before the subsequent noises are
computed.

.. jupyter-execute::

    model.modes("off")
    model.fsig.f = 1
    fdetune = np.arange(-40, -30, 2)[::-1]  # FC detuning frequencies [Hz]
    fmin = 5
    fmax = 5e3
    npts = 200
    F_Hz = np.geomspace(fmin, fmax, npts)

    def qnoise_sweep(name):
        return fa.Xaxis(
            model.fsig.f, "log", fmin, fmax, npts - 1, name=name,
        )

    with model.temporary_parameters():
        sol = model.run(
            fa.Series(
                # lock the interferometer and transition to DC readout
                aligo.InitialLock(),
                aligo.DARM_RF_to_DC(),
                # calculate the sensing function
                fa.FrequencyResponse(F_Hz, "DARM.AC.i", "AS.DC", name="sensing"),
                # calculate the quantum noise for a range of FC detunings
                fa.TemporaryParameters(
                    fa.For(
                        "FC_detune_Hz",
                        fdetune,
                        qnoise_sweep("qnoise"),
                    ),
                ),
                # frequency dependent squeezing
                qnoise_sweep("fd_sqz"),
                # misalign the FC end mirror for frequency independent squeezing
                fa.Change({"FC2.misaligned": True}),
                qnoise_sweep("fi_sqz"),
                # change squeeze angle for frequency independent anti-squeezing
                fa.Change({"SQZ.angle": 90}),
                qnoise_sweep("fi_asqz"),
                # no squeezing
                fa.Change({"SQZ.db": 0}),
                qnoise_sweep("no_sqz"),
            ),
        )

The quantum noise is calculated in units of :math:`\mathrm{W}/\mathrm{Hz}^{1/2}`. To
calibrate the noise into equivalent displacement in units of
:math:`\mathrm{m}/\mathrm{Hz}^{1/2}`, we divide by the sensing function as calculated by
the :external:class:`~finesse.analysis.actions.lti.FrequencyResponse` action.

.. jupyter-execute::

   plant = np.abs(sol["sensing"].out.squeeze())
   sqz = lambda k: sol[k, "AS_p1_qnoise"]

   axs = bode(F_Hz, sol["sensing"].out.squeeze(), db=False)
   axs[0].set_ylabel("Magnitude [W / m]")
   axs[0].set_title("DARM sensing function")

   fig, ax = plt.subplots()
   ax.loglog(F_Hz, sqz("fd_sqz") / plant, label="Freq. Dep. SQZ")
   ax.loglog(F_Hz, sqz("fi_sqz") / plant, label="Freq. Indep. SQZ")
   ax.loglog(F_Hz, sqz("fi_asqz") / plant, label="Freq. Indep. ASQZ")
   ax.loglog(F_Hz, sqz("no_sqz") / plant, label="No SQZ")
   ax.legend()
   ax.set_xlabel("Frequency [Hz]")
   ax.set_ylabel("Displacement [m/Hz$^{1/2}$]")
   ax.set_title("Quantum Noise");

At the time of this writing, the filter cavity finesse is not optimal for the
circulating arm power. The injected squeezed state thus does not have the correct
frequency dependence to perfectly cancel the rotation caused by the ponderomotive
squeezing in the interferometer.

We can also plot the noise as dB relative to the noise caused by the unsqueezed vacuum
entering the dark port by dividing by the quantum noise with no squeezing. Note that
this `includes the effects of radiation pressure in the interferometer` and is thus not
the noise relative to shot noise at the low frequencies where radiation pressure is
dominant.

.. jupyter-execute::

   to_dB = lambda k: 20 * np.log10(sqz(k) / sqz("no_sqz"))
   fig, ax = plt.subplots()
   ax.semilogx(F_Hz, to_dB("fd_sqz"), label="Freq. Dep. SQZ")
   ax.semilogx(F_Hz, to_dB("fi_sqz"), label="Freq. Indep. SQZ")
   ax.semilogx(F_Hz, to_dB("fi_asqz"), label="Freq. Indep. ASQZ")
   ax.legend()
   ax.set_xlabel("Frequency [Hz]")
   ax.set_ylabel("Noise relative to vacuum [dB]");

Finally we plot the quantum noise for a range of filter cavity detunings.

.. jupyter-execute::

   sqz_fc = sol["for", "qnoise", "AS_p1_qnoise"] / plant
   fig, ax = plt.subplots()
   for idx, detune in enumerate(fdetune):
       ax.loglog(F_Hz, sqz_fc[idx], label=f"{detune:0.0f} Hz")
   ax.legend()
   ax.set_xlabel("Frequency [Hz]")
   ax.set_ylabel("Displacement [m/Hz$^{1/2}$]");
