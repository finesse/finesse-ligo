.. include:: /defs.hrst
.. _factory_input_output_matrices:


LSC Matrices
------------

Length sensing and control input and output matrices control the AC signal flow
coming from readouts to combine to make an error signal for a degree of freedom.
The output matrices describe what length degree freedoms drive which :ref:`local
drives <factory_local_drives>`.

For LSC features to be added the factory must be set to use
:code:`factory.options.LSC.add = True`.

Input Matrices
==============

.. jupyter-execute::

    import pprint
    from finesse_ligo.factory import aligo
    factory = aligo.ALIGOFactory("llo_O4.yaml")

    pprint.PrettyPrinter(indent=0,width=20).pprint(
        factory.LSC_input_matrix.toDict()
    )

Output Matrices
===============

.. jupyter-execute::

    import pprint
    from finesse_ligo.factory import aligo
    factory = aligo.ALIGOFactory("llo_O4.yaml")

    pprint.PrettyPrinter(indent=0,width=20).pprint(
        factory.LSC_output_matrix.toDict()
    )


ASC Matrices
------------

Similar to above for the LSC but instead these describe the angular inputs and outputs.
For ASC features to be added the factory must be set to use
:code:`factory.options.ASC.add = True`.

Input Matrices
==============
.. jupyter-execute::

    import pprint
    from finesse_ligo.factory import aligo
    factory = aligo.ALIGOFactory("llo_O4.yaml")

    pprint.PrettyPrinter(indent=0, width=20).pprint(
        factory.ASC_input_matrix.toDict()
    )


Output Matrices
===============

.. jupyter-execute::

    import pprint
    from finesse_ligo.factory import aligo
    factory = aligo.ALIGOFactory("llo_O4.yaml")

    pprint.PrettyPrinter(indent=0, width=20).pprint(
        factory.ASC_output_matrix.toDict()
    )


Input matrix scalar or transfer function
=====================================

The input matrix elements can either be a scalar value or 4-tuple which consists
of a :code:`(filter_name, z, p, k)`. These options can be used to sum multiple
scaled readouts, or sum filtered outputs using some zpk filter.

.. jupyter-execute::

    import finesse
    import numpy as np
    finesse.init_plotting()

    from finesse_ligo.factory import aligo
    factory = aligo.ALIGOFactory("llo_O4.yaml")

    factory.options.ASC.add = True
    factory.ASC_input_matrix['DHARD_P'] = {
        'REFL_A_WFS9x.I': ('my_filter', [], [2*np.pi*100j], 1)
    }

    model = factory.make()
    model.my_filter.bode_plot()

Remember, you can always change the zpk values after making the model. The
factory will just use these initial settings to connect up the signal flow from
readouts, to controllers, and sum the various signals.


Output matrix variables and constants
=====================================

Looking at the above for ASC you'll notice that some of the scaling factors are
set as strings. This means that when the ASC is added to the model it will end
up making a new model parameter with that name. This means that the coupling
here can be controlled later by using a value, or setting it to some expression.
We can see this below where the :math:`rx_{P}` value is set to some complicated
expression that will evaluated at runtime. This also means that as the user
changes other values in the simulation this parameter will follow.

.. jupyter-execute::

    factory.reset()
    factory.options.ASC.add = True

    model = factory.make()

    print(factory.ASC_output_matrix['DHARD_P'])
    print(model.rx_P)
