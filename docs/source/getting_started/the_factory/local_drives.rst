.. include:: /defs.hrst
.. _factory_local_drives:

Local Drives
------------

:code:`factory.local_drives` describes what signal nodes are used at each of the
main optical components to connect AC loops to and from. There are length,
:code:`L`, pitch, :code:`P`, and yaw, :code:`Y`. These nodes should be amended
if you want AC signals to be driven to forces or displacements as you need.

.. jupyter-execute::

    import pprint
    from finesse_ligo.factory import aligo
    factory = aligo.ALIGOFactory("llo_O4.yaml")

    pprint.PrettyPrinter(indent=0,width=20).pprint(
        factory.local_drives.toDict()
    )

The dictionary for the length motion of the BS looks like :code:`'BS':
{'BS.mech.z': 1}`. This means the BS element length AC motion is sent to the
BS mechanical node :code:`z`, which is a displacement.

Note that this is a dictionary and the signal can be sent to multiple nodes, for
example :code:`'BS': {'BS.mech.z': 1, 'BS.mech.yaw': 1}` would send equal parts
of the AC length signal to the z and yaw motion.


.. todo::
    Add details on setting filters for the drives
