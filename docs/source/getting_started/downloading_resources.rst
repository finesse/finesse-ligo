.. include:: /defs.hrst
.. _downloading_resources:

Downloading resources
=====================

The |Finesse| LIGO tools not only relies on the |Finesse| v3 package but also on
many other datafiles, such as surface maps, suspension models, etc. The
available downloads

.. jupyter-execute::

    import finesse_ligo
    import pprint

    pprint.PrettyPrinter(indent=0,width=20).pprint(
        finesse_ligo.tools.DATAFILES
    )

To download a file you can use the inbuilt command, for example to download
ETM10 map files you can use
:code:`finesse_ligo.tools.download('ETM10.h5')`. You should see some download
information like:

.. code-block:: console

    Writing data to /Users/me/finesse-data/finesse-ligo
    Downloading https://dcc.ligo.org/public/0182/T2200149/001/ETM10.h5: 3.57M/3.57M [00:02<00:00, 1.54MB/s]

This shows you where the file is being downloaded to (and where
:code:`finesse_ligo` will look for these files). The :code:`DATAFILES`
dictionary shown above also shows where these files will be downloaded from. You
can follow these URLs to find out more about each package.
