.. include:: /defs.hrst
.. _length_locking:

Operating points & length locking
=================================

The number one difficulty people have with working on the LIGO models is
ensuring that the model is at a good "operating point". For the length operating
point we are typically concerned with ensuring the microscopic tunings of every
cavity is at a value which ensures the interferometer performs as required.
There is no singular ideal operating point, you as a user must decide how you
want the interferometer to behave and choose a control scheme to get you there,
for example do you want to use signal recycled or resonant sideband extraction?
Each has its own operating point, or positioning requirements of the mirrors.
It's important to note that |Finesse| cannot do this design step for you.

The ethos |Finesse| uses is that you should use realistic error signals to find
an operating point. In practice this means you must define a radio-frequency or
dither control scheme that generates some error signals, you then position your
mirrors so that all the relevant error signals are "zero" - the collection of
mirrors that are DC positioned is what we refer to as an operating point. These
operating points will vary as a function of all the other interferometer
parameters, for example mode-matching, thermal effects, misalignments, etc.
Therefore whenever you change some of these parameters you need to then ensure
that you are still at an acceptable operating point.

This is what makes modelling complex systems challenging and time consuming. If
you don't do this step your models are very likely to just be wrong, this is
not a step you should skip.

Here we will cover what DC locking in |Finesse| means, how to lock and debug
locks, and how to transition DARM from RF to DC readout.


DC locking
----------

As |Finesse| is a steady-state frequency domain modelling tool, the first step
is to DC position the relevant mirrors to find your operating point. At that
point you can then compute various detector outputs or perform AC analyses.
We call this step `DC locking`. The DC refers to the fact we are not providing
any control loop feedback to control AC frequencies, i.e. mirror motions at
100Hz. This is a useful simplification as often you do not need the full AC
control loops unless you are studying stability problems or projecting noise.

In order to find the DC operating point we require a control scheme. For LIGO
this is fairly well defined, the sites may have small differences between them,
but broadly we control 5 length degrees of freedom:

* PRCL - RF sensed at POP, 9MHz
* SRCL - RF sensed at POP, 45MHz
* MICH - RF sensed at POP, 45MHz
* CARM - RF sensed at REFL, 9MHz
* DARM - Either RF sensed as AS, 45MHz, or using DC readout

.. warning::
    Don't confuse "DC locking" with "DC readout for DARM", in |Finesse| we
    distinguish between the AC and DC locking of degrees of freedom. AC locking
    requires setting up some signal feedback path and isn't always necessary to
    perform most simulations. DC locking is always needed.


Locking command
---------------

The :external:class:`finesse.locks.Lock` element describes what error signal is
used to control which degree of freedom for DC locking. First lets make a model:

.. jupyter-execute::

    from finesse_ligo.factory import aligo
    factory = aligo.ALIGOFactory("lho_O4.yaml")
    model = factory.make()
    model.modes(maxtem=2) # use at least up to 2nd order modes to include mismatch

We can see what DC locks are present in the model by checking the collection of locks:

.. jupyter-execute::

    model.locks

We should see there are locks for the degrees of freedom above. Taking CARM as
an example we can check what error signal is used and where it is feeding back to.

.. jupyter-execute::

    model.CARM_lock.error_signal, model.CARM_lock.feedback

CARM uses the REFL 9MHz signal and feedsback into the CARM degree of freedom.
CARM drives ETM X and Y in a common way, with a scaling factor of 1/2.

.. jupyter-execute::

    model.CARM.drives, model.CARM.amplitudes

The locking algorithm in |Finesse| is essentially an N-dimensional root finder,
looking for the zero crossings in every error signal for a lock that is enabled.
We can check which locks are enabled by just searching through the locks added:

.. jupyter-execute::

    [lock for lock in model.locks if lock.enabled]

We can see at this stage we are using an RF lock for DARM, rather than DC
readout.

The locking algorithm is essentially a deadband controller. It will take what
the current error signals are, multiply them by the gain parameter set for each
:external:class:`lock <finesse.locks.Lock>` and then increment the feedback
target by this value. It will do this until all the error signals are below the
:external:class:`lock <finesse.locks.Lock>` accuracy values. This will typically
be tried a set number of times, if the error signals do not converge then
|Finesse| will state that it has lost the lock. In these cases a
:external:class:`finesse.exceptions.LostLock` exception will be raised.


The locking action
------------------

The :external:class:`lock <finesse.locks.Lock>` elements merely state at a
higher level how the DC state is to be controlled. To actually run the locks a
simulation must be started and the
:external:class:`RunLocks <finesse.analysis.actions.locks.RunLocks>` action must be
applied. Some important options for this action to note are:

* :code:`max_iterations` - sets how many iterations to try before assuming we
  have lost lock. Obviously it could be that the lock is taking too long to
  converge if the gains are set too low.
* :code:`exception_on_fail` - When set to true, a
  :external:class:`finesse.exceptions.LostLock` will be raised, if False, the
  lock will continue even though everything has technically failed. This is
  useful because you can then retrieve the output for debugging what went wrong.


Initial Locking
---------------

The Lock action above is one part of the process of finding a good DC operating
point. As with many other optimisation methods, the initial condition to start
the locking algorithm from, as well as the initial gain settings must be chosen
carefully.

An initial locking algorithm has been developed that is loosely based on the
locking process of the real detectors. The idea is to lock cavities sequentially
hopefully landing us in a relatively good place that the full global lock
optimisation is successful. This algorithm is not fool proof and is not
guaranteed to work, if the model is setup too far form something that is
sensible or even operable.

The function :meth:`finesse_ligo.factory.aligo.InitialLock` will create an
initial locking action for us to use with models generated by the factory.

.. jupyter-execute::

    init_lock = aligo.InitialLock()
    # print what this action is going to do
    print(init_lock.plan())

We can see there are multiple steps involved. You'll notice that the locks are
only ran right at the end, after various other optimisation steps have been
performed and then the demodulation phases and lock gains are computed.
The first term in :code:`misalign PRM,SRM - Change`, is the name of the action
and the last one is the class, in this case it's a :external:class:`Change
<finesse.analysis.actions.random.Change>` action.

We can apply the action by running it on the model:

.. jupyter-execute::

    lock_sol = model.run(init_lock)
    print(lock_sol)

When we print the solution we find it is a series of solutions from each of the
steps in the initial lock action. We can pick out individual ones by name
:code:`lock_sol['lock XARM']` to study further, if we need to.

.. jupyter-execute::

    # For example, seeing how many iterations the final run lock step needed
    lock_sol['run locks'].iters

Operating point checks
----------------------

DC power check
~~~~~~~~~~~~~~

There are various ways you can check if you are at a good operating point, the
most simple and first order check is that the DC powers and buildups are
correct. We just run the base model, which will compute a single DC data point:

.. jupyter-execute::

    sol = model.run()
    print("P XARM   :", sol['Px'])
    print("P YARM   :", sol['Py'])
    print("PRG      :", sol['PRG'])
    print("PRG 9    :", sol['PRG9'])
    print("PRG 45   :", sol['PRG45'])
    print("XARM Gain:", sol['AGX'])
    print("YARM Gain:", sol['AGX'])

Here we should see a good power recycling and arm gains.

Error signal check
~~~~~~~~~~~~~~~~~~

A slightly more involved check is plotting all the error signals being used.
First we make an action that will sweep each degree of freedom around its
current value. After that we normalise and plot the error signals. What we want
to see is that they are all crossing 0 for zero offsets. Note that due to the
differing linewidths of the various cavities we need to manually state what
range to scan over.

.. jupyter-execute::

    import finesse
    finesse.init_plotting()

    import finesse.analysis.actions as fac
    import matplotlib.pyplot as plt
    import numpy as np

    error_signal = fac.Series(
        fac.Xaxis(model.CARM_lock.feedback, 'lin', -0.1, 0.1, 100, relative=True, name=model.CARM_lock.name),
        fac.Xaxis(model.DARM_rf_lock.feedback, 'lin', -0.1, 0.1, 100, relative=True, name=model.DARM_rf_lock.name),
        fac.Xaxis(model.PRCL_lock.feedback, 'lin', -1, 1, 100, relative=True, name=model.PRCL_lock.name),
        fac.Xaxis(model.SRCL_lock.feedback, 'lin', -20, 20, 100, relative=True, name=model.SRCL_lock.name),
        fac.Xaxis(model.MICH_lock.feedback, 'lin', -5, 5, 100, relative=True, name=model.MICH_lock.name),
    )

    def plot_error_signals(model, errsig_sols):
        plt.figure()
        for lock in model.locks:
            if lock.enabled:
                sol = errsig_sols[lock.name]
                x = sol.x[0]/sol.x[0].max()
                y = sol[lock.error_signal.name]
                y /= y.max()
                plt.plot(x, y, label=lock.error_signal.name)
        plt.legend()
        plt.xlabel("Offset [arb]")
        plt.ylabel("Error signal [arb]")

    errsig_sols = model.run(error_signal)
    plot_error_signals(model, errsig_sols)


Plotting the locking process
----------------------------

It's useful to make some plots about what the locking process is doing and what
might be failing. If you get the :external:class:`RunLocksSolution
<finesse.analysis.actions.locks.RunLocksSolution>` solution this contains what
the lock did or tried to do.

.. jupyter-execute::

    # perturb CARM is in degrees of phase:
    #    0.1*np.pi/180/model.k0 [m] ~ 0.3nm
    model.CARM.DC += 1e-2
    lock_sol = model.run(fac.RunLocks())

    plt.figure()
    lock_sol.plot_control_signals()

The first plot shows how the control signals, or the signals being sent to the
feedback, are evolving for each step. We should see a nice converge with the
steps getting smaller each time. You might notice that the control signals
occasionally go to zero, this is the deadband controller at work when a
particular error signal is within tolerance.

.. jupyter-execute::

    plt.figure()
    lock_sol.plot_error_signals()

We can then also look at what the error signals are doing. Again we should see
them reduce if the locks are working well.

When locks go wrong
-------------------

Locks can go wrong for a variety of reasons:

* The lock gain is incorrect and the system is unstable
* The error signal has degraded
* There is cross couplings between error signals and the locks fight each other
  (Sensing scheme chosen is bad)

Any of these can happen as you run a simulation too, for example if you're
studying how thermal effects develop.

Here we introduce a disturbance but purposefully set the lock gain too high.
As we know it will fail, we also limit the number of steps and tell it not to
raise an exception so we can retrieve the result.

.. jupyter-execute::

    with model.temporary_parameters():
        model.CARM.DC += 0.01
        model.CARM_lock.gain *= 5
        lock_sol = model.run(fac.RunLocks(max_iterations=500, exception_on_fail=False))
        lock_sol.plot_error_signals()
        DC = model.run()
        print(DC['Px'], DC['PRG'])

We can see there is a quick burst of activity followed by a decline. The problem
with just relying on signal samples of the error signal is that we do not know
if we have left the linear region of the error signal and now we are finding
some other incorrect error signal zero. If we calculate the arm power and PRG we
see it is far too low.

To get a better understanding of what might be going on we can plot the error
signals,

.. jupyter-execute::

    with model.temporary_parameters():
        model.CARM.DC += 0.01

        errsig_sols = model.run(error_signal)
        plot_error_signals(model, errsig_sols)

which we can see look terrible. There are huge offsets present in the error
signals, which if the gain is now too high the model will take a large step and
inadvertently leave the linear regime of the error signals and we will loose
lock.

You'll notice though that if you don't over gain the locks it does still re-lock
to a good operating point, eventually, even with a large initial displacement.

.. jupyter-execute::

    with model.temporary_parameters():
        model.CARM.DC += 0.1
        lock_sol = model.run(fac.RunLocks())
        lock_sol.plot_error_signals()

        DC = model.run()
        print(DC['Px'], DC['PRG'])

This has the side-effect that locks take longer to converge when we do have smaller
disturbances. As one can imagine, there is now a careful balance between how
large a steps you want to make in a simulation and how high your gains should be
to deal with them, making them too small results in a long running simulation.
You can also experiment with the accuracy of the locks too. Often you need more
less accurate locks if you're only interested in DC power buildups. If you're
computing AC transfer functions then you maybe need more accurate locks, as they
are more sensitive to detuned cavities, especially with radiation pressure
effects at lower frequencies.

Switch DARM from RF to DC readout
---------------------------------

By default the models are setup to run with RF locking for DARM. Why? Because
it's more robust to changes you make in the model and is a good way to ensure
that you reach a good operating point. Once you are at a good operating point,
you can quickly just switch to DC readout.

.. jupyter-execute::

    print(aligo.DARM_RF_to_DC().plan())

This runs a custom action :class:`DARM_RF_to_DC
<finesse_ligo.factory.aligo.DARM_RF_to_DC>` which switches some settings over,
it then finally runs the new DC lock. We can plot the error signal and see that
we have a quadratic response and see that the zero-point is at the requested
locking offset.

.. jupyter-execute::

    model.run(aligo.DARM_RF_to_DC())

    darm_error = model.run(
        fac.Xaxis(
            model.DARM_rf_lock.feedback,
            'lin',
            -1e-3, 1e-3, 100,
            relative=True
        )
    )

    plt.plot(darm_error.x[0] * np.pi/180/model.k0/1e-12, darm_error['AS_DC']/1e-3)
    plt.xlabel("DARM [pm]")
    plt.ylabel("Power [mW]")

    print(model.DARM_dc_lock.offset)
