.. include:: /defs.hrst
.. _cavities:

Cavities
========

Optical cavities are the main building blocks of the models and where most of
the complication lies. Cavities have two meanings in |Finesse|, the first are
just the ways in which the optical model is connected. If you have connected the
elements in such a way there is a loop in the optical couplings, there will be a
cavity. This will be simulated when you run a simulation.

The second part of the cavity definition is used for beam tracing, or setting
the shape of the optical fields being used throughout the model. This is where
the :external:class:`Cavity <finesse.components.cavity.Cavity>` element in
|Finesse| comes in. This element will perform a
roundtrip ABCD trace through a set of components you choose and uses that
cavitiy spatial eigenmode as the basis in which to use for generating higher
order modes.

Looking at the LIGO model you will see it generates several cavities:

.. jupyter-execute::

    import finesse_ligo
    from finesse_ligo.factory import aligo

    factory = aligo.ALIGOFactory("lho_O4.yaml")
    model = factory.make()

    model.cavities

The ordering here states the priority of the cavity. |Finesse| will use these
cavitiy eigenmodes to trace to nodes in the model that have not been covered.
In this case you'll see the OMC as the first one, therefore every node between
the OMC and the SRM will be using the OMCs eigenshape as a reference. This also
means that a mismatch will happen at the SRM interface, as the priority tracing
stops once it reaches a new cavity.

The exact choise of priority shouldn't matter, but it can affect how you
interpret higher order modes in your simulation. The default one typically
works. The OMC mode is used for the output, then the arm cavities, then the
other cavities.

.. jupyter-execute::

    model.gausses

You should also check what Gaussian beam parameters have been set to, in this
case with the default options you'll notice we do not have an IMC cavity but
instead have an IMC Gaussian parameter setting. This is computed from the IMC by
the factory and for model simplicity, the IMC has not been included as you do
not often need it. This parameter is set at IM2.

.. jupyter-execute::

    model.gIM2_p1_i.qx, model.gIM2_p1_i.qy


Input mode cleaner (IMC)
------------------------

The factory can be customised to include many features. With some changes we can
build a model that does include the IMC:

.. jupyter-execute::

    print(factory.options.INPUT)
    factory.options.INPUT.add_IMC_and_IM1 = True
    model = factory.make() # make a new model
    print("Cavities", model.cavities)
    print("Gausses", model.gausses)

We now see we have an IMC and no Gaussian beam parameters being set. This is
because the IMC is now defining our input mode.

We can dig into this cavity element to find out more about the IMC, showing what
elements it contains:

.. jupyter-execute::

    model.cavIMC.path.components

Or what nodes it has traced for the path

.. jupyter-execute::

    model.cavIMC.path.nodes

The cavity element also does some calculations for us too, and we can get a
summary in a table form. Each of these parameters can also be computed separately.

.. jupyter-execute::

    model.cavIMC.info_parameter_table()

This is useful for sanity checking some numbers in your model when you first use
it. Remember that terms like waist size/position are defined relative to the
`cavity.source` node.

X & Y arm cavities (XARM & YARM)
--------------------------------

The arm cavities are fairly straight forward as they are simple fabry-perot
cavities. The parameters for the XARM:

.. jupyter-execute::

    print("XARM components")
    display(model.cavXARM.path.components)

    model.cavXARM.info_parameter_table()

and YARM:

.. jupyter-execute::

    print("YARM components")
    display(model.cavYARM.path.components)

    model.cavYARM.info_parameter_table()


Power recycling cavity (PRC)
----------------------------

The power recycling cavity is formed between the PRM and the two ITMs, ITMX and
ITMY. This split nature means the single cavity element description does not
apply well. You'll notice we need to define PRX and PRY between the X and Y ITMs
respectively. It's important to note that this single cavity picture is wrong,
the PRC features are heavily defined by its coupling to the arm cavities and the
fact PRX and PRY and co-joined at the BS.

.. jupyter-execute::

    print("PRX components")
    display(model.cavPRX.path.components)

    model.cavPRX.info_parameter_table()

You'll notice that some of these numbers do not make sense. This is because this
simple single cavity picture is seeing a 50% loss at the BS, therefore
calculations like the optical gain are wrong.

It should also be noted that the Gouy phase is also complicated as the real Gouy
phase is set by the fully resonant coupled PRC. However, it is often useful to
query the PRX and PRY properties to ensure they are not drastically different.
The more different they are the mode HOMs you will likely need to describe the
coupled cavity.


Signal recycling cavity (SRC)
-----------------------------

The SRC is similar to the PRC, the numbers should be carefully considered due to
its split coupled nature.

.. jupyter-execute::

    print("SRX components")
    display(model.cavSRX.path.components)
    print("SRY components")
    display(model.cavSRY.path.components)


Output mode cleaner (OMC)
-------------------------
