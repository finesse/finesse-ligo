.. include:: /defs.hrst
.. _the_model:

The Model
=========

In this section we will cover the various levels of components that are included
in the LIGO models. Often parts of the model are connected together in
complicated ways so it is worth understanding each part and what it does.


.. toctree::
    :maxdepth: 2

    length_locking
    cavities
    tracing_beams
    angular_sensing_control
    thermal
