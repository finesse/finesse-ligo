.. include:: ../defs.hrst
.. _getting_started:

Getting Started
===============

Here you will find a selection of examples for performing various simulation tasks
using |Finesse| LIGO.

.. toctree::
    :maxdepth: 2

    basics
    the_model/index
    the_factory/index
    downloading_resources
